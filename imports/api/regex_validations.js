/**
 * Created by ikedi on 26/07/2016.
 */

let ck_name = /^[A-Za-z0-9 ]{3,20}$/;
let ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
let ck_password =  /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
let ck_phone = /^\d{11}$/; //validate that phone is 11 digits

let regex_validations = {
    ck_name,
    ck_email,
    ck_password,
    ck_phone
}

export default  regex_validations;