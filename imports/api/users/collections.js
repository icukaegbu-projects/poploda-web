/**
 * Created by ikedi on 23/09/2016.
 */
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

//collection for Transactions
export const Transactions = new Mongo.Collection('transactions');
Transactions.schema = new SimpleSchema({
    //customer data
    'cust_id': { type: String }, //HOLDS userId if registered, else null
    'cust_name': { type: String}, //HOLDS user email
    'userId': { type: String, defaultValue: '' },
    'isRegistered': { type: Boolean, defaultValue: false },
    //product data
    'product_id': { type: String },
    'amount': { type: Number },
    'currency': { type: Number },
    'txn_time': { type: String },
    'txn_ref': { type: String },
    'hash': { type: String },
    'pay_item_id': { type: String },
    'network': { type: String },
    'phone': { type: String },
    //transaction data
    'txn_status': { type: String, optional: true },
    'pay_ref': { type: String, optional: true },
    'ret_ref': { type: String, optional: true },
    'merchant_ref': { type: String, optional: true },
    'card_number': { type: String, optional: true },
    'retrieval_ref_no': { type: String, optional: true },
    'lead_bank_code': { type: String, optional: true },
    'lead_bank_name': { type: String, optional: true },
    'response_code': { type: String, optional: true },
    'response_description': { type: String, optional: true }
});

export const EmailBlast = new Mongo.Collection('email_blasts');
EmailBlast.schema = new SimpleSchema({
    'content': { type: String },
    'recipients': { type: String },
    'recipient_count': { type: Number },
    'recipient_phone_numbers': { type: Array[Number] },
    'date_sent': { type: Date }
});