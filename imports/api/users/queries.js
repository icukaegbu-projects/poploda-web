/**
 * Created by ikedi on 10/09/2016.
 */
import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { HTTP } from 'meteor/http';
import jsSHA from 'jssha';
import { Transactions } from './collections';

//const INTERSWITCH_URL = 'https://testwebpay.interswitchng.com/test_paydirect/pay';
//const INTERSWITCH_URL = 'https://tlsdebug.interswitchng.com/';
//const INTERSWITCH_URL = 'https://webpay.interswitchng.com/paydirect/pay'
// const INTERSWITCH_URL = 'https://stageserv.interswitchng.com/test_paydirect/pay';
// const INTERSWITCH_GETTRANSCTION_URL="https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json";
// const AIRVEND_URL = 'https://api.airvendng.net/vtu/';

export const STATUS = {
    'Transaction_Initiated': 'TXN_INITIATED',
    'Payment_Transferred': 'FUNDS_TRANSFERRED',
    'Transaction_Completed': 'TXN_COMPLETED'

    // 'Transaction_Initiated': 'TRANSACTION_INITIATED_NO_FUNDS_TRANSFERED',
    // 'Payment_Transfered': 'TRANSACTION_INITIATED_FUNDS_TRANSFERED',
    // 'Transaction_Completed': 'TRANSACTION_COMPLETED_AIRTIME_LOADED'
}

const callService = (type, url, options) => new Promise((resolve, reject) => {
    if(Meteor.isServer){
        HTTP.call(type, url, options, (error, result) => {
            if(error) {
                reject(error);
            } else {
                resolve(result);
            }
        });
    }
});

const stringifyQueryParams = (queryParams) => {
    let query = '';

    for(let key in queryParams){
        query += `${key}=${queryParams[key]}&`
    }
    // console.log(query)
    // console.log(query.slice(0, query.length-2))
    return query.slice(0, query.length-1)
}

export const hashSHA512 = (hashString) => {
    // let SHA512 = createHash('sha512');
    // let hash = SHA512.update(hashString, 'utf-8').digest('hex');
    // console.log('HASH: '+hash.toString())
    // return hash.toString();

    var shaObj = new jsSHA("SHA-512", "TEXT");
    shaObj.update(hashString);
    var hash = shaObj.getHash("HEX");
    return hash.toString();
}

export const queryAirVendAPI = new ValidatedMethod({
    name: 'queries.queryAirVendAPI',
    validate: new SimpleSchema({
        // queryParams: { type: Object, blackbox: true },
        'msisdn': { type: String },
        'networkid': { type: Number },
        'amount': { type: Number },
        'type': { type: Number },
        'ref': { type: String },
        'username': { type: String },
        'password': { type: String }
    }).validator(),
    run (queryParams){
        console.log('Inside queryAirVendAPI');
        console.log(queryParams);
        let query = stringifyQueryParams(queryParams);
        console.log(query)
        return callService(
            'POST',
            Meteor.settings.private.AIRVEND_URL,
            { query }
        ).then((result) => {
            //console.log('From then');
            console.log('from queryAirVend');
            console.log(result)
            return result;
        }).catch((error) => {
            throw new Meteor.Error('500', `${error.message}`);
        });
    }
});

export const queryInterSwitch = new ValidatedMethod({
    name: 'queries.queryInterSwitch',
    validate: new SimpleSchema({
        'product_id': { type: String },
        'amount': { type: Number },
        'currency': { type: Number },
        'site_redirect_url': { type: String },
        'txn_ref': { type: String },
        'hash': { type: String },
        'pay_item_id': { type: String }
    }).validator(),
    run (queryParams){
        console.log('Calling Interswitch');
        console.log(queryParams);
        let query = stringifyQueryParams(queryParams)

        return callService(
            'POST',
            Meteor.settings.private.INTERSWITCH_URL,
            query
        ).then((result) => {
            //console.log('From then');
            //console.log(result);
            return result;
        }).catch((error) => {
            console.log(error);
            throw new Meteor.Error('500', `${error.message}`);
        });
    }
});

export const queryInterSwitchGetTransaction = new ValidatedMethod({
    name: 'queries.queryInterSwitchGetTransaction',
    validate: new SimpleSchema({
        'product_id': { type: String },
        'amount': { type: Number },
        'txn_ref': { type: String },
        'mac_key': { type: String },
        'phone': { type: String },
        'network': { type: String }
    }).validator(),
    run (queryParams){
        //set the hash in th header
        //call getTransaction with JSON
        //check the response code
        // if successful, update the status in the DB wth the result
        //
        console.log('Calling GetTransaction');
        console.log(queryParams);
        let query = stringifyQueryParams({
            productId: queryParams.product_id,
            transactionReference: queryParams.txn_ref,
            amount: queryParams.amount
        })
        let headers = {
            hash: hashSHA512(queryParams.product_id+queryParams.txn_ref+queryParams.mac_key)
        }
        console.log(query);

        return callService(
            'GET',
            Meteor.settings.private.INTERSWITCH_GETTRANSCTION_URL,
            { query, headers }
        ).then((result) => {
            //verify if successful ie code = 00 and update DB with details (change status)
            //if yes, call AirVend to buy credit
            //sif(result)
            //CHECK THE STATUSCODE OF THE RETURNED RESULT; IF SUCCESSFUL,
            //UPDATE TRANSACTION STATUS TO FUNDS_TRANSFERRED AND
            //CALL AIRVEND, IF NOT TERMINATE
            //USE RESULT TO UPDATE TRANSACTION DETAILS
            console.log('from getTransaction-result: '+JSON.stringify(result));
            Transactions.update()
            let fields = {
                network: queryParams.network,
                phone: queryParams.phone,
                amount_naira: queryParams.amount / 100,
                ref: queryParams.txn_ref,
            }

            let creditPurchase = buyCreditFromAirVend(fields);
            console.log('from buyCreditFromAirvend: '+JSON.stringify(creditPurchase));
            return creditPurchase;
        }).catch((error) => {
            console.log('from getTransaction-error: '+JSON.stringify(error));
            throw new Meteor.Error('500', `${error.message}`);
        });
    }
});

function buyCreditFromAirVend(fields) {
    let networkid = 0, network = fields.network.toUpperCase();
    console.log(fields.network)

    switch (network) {
        case 'AIRTEL':
            networkid = 1;
            console.log(networkid);
            break;
        case 'MTN':
            networkid = 2;
            console.log(networkid);
            break;
        case 'GLO':
            networkid = 3;
            break;
        case 'ETISALAT':
            networkid = 4;
            break;
        case 'VISAPHONE':
            networkid = 5;
            break;
        default:
            networkid = 6;
    }

    let queryParams = {
        msisdn: fields.phone,
        networkid: networkid,
        amount: fields.amount_naira,
        type: 1,
        ref: fields.ref,
        username: Meteor.settings.private.AIRVEND_USERNAME,
        password: Meteor.settings.private.AIRVEND_PASSWORD
    }

    queryAirVendAPI.call(queryParams,  (error, result) => {
        if (error) {
            console.log('AirVend API error: ', error);

            throw new Meteor.Error('AirVend API Error', error)
        } else {
            console.log('Purchase from Airvend complete, redirect!');
            console.log(result);

            return result;
        }
    });
}
