/**
 * Created by ikedi on 09/08/2016.
 */
import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { HTTP } from 'meteor/http';
import { Email } from 'meteor/email';

import { queryInterSwitchGetTransaction } from './queries';
import { Transactions, EmailBlast } from './collections';

const confirmationEmail = (email, password, phone) => {
    let mail = `Welcome to Poploda\n. Your login details are:\n Email: ${email}\n Password: ${password}\n Phone: ${phone}\n`;
    return mail;
}

export const userDelete = new ValidatedMethod({
    name: 'users.delete',
    validate: new SimpleSchema({
        _id: { type: String }
    }).validator(),
    run({ _id }) {
        return Meteor.users.remove(_id);
    }
});

export const userUpdate = new ValidatedMethod({
    name: 'users.update',
    validate: new SimpleSchema({
        _id: { type: String },
        bio: { type: Object },
        'bio.name': { type: String },
        'bio.phone': { type: String },
        'bio.network': { type: String },
    }).validator(),
    run({ _id, bio }) {
        //first check if the number exists, if yes throw error and exit
        // if(Meteor.isServer){
        //     let exists = Accounts.findUserByUsername(bio.phone);
        //     if(exists) {
        //         throw new Meteor.Error(300, 'Phone number exists');
        //     }
        //
        //     let success = Meteor.users.update({_id}, {
        //         $set: {
        //             username: bio.phone,
        //             'bio.name': bio.name,
        //             'bio.phone': bio.phone,
        //             'bio.network': bio.network
        //         }
        //     });
        //     return success;
        // }

        let exists = Meteor.users.findOne({ username: bio.phone });

        if(exists) {
            throw new Meteor.Error(300, 'Phone number exists');
        }

        let success = Meteor.users.update({_id}, {
            $set: {
                username: bio.phone,
                'bio.name': bio.name,
                'bio.phone': bio.phone,
                'bio.network': bio.network
            }
        });
        return success;
    }
});

export const userSignup = new ValidatedMethod({
    name: 'users.signup',
    validate: new SimpleSchema({
        _id: { type: String, optional: true },
        emails: { type: Array },
        'emails.$': { type: Object },
        'emails.$.address': { type: String },
        'emails.$.verified': { type: Boolean },
        password: { type: String },
        name: { type: String },
        phone: { type: String },
        network: { type: String },
        birthday: { type: String, optional: true },
        createdAt: { type: Date, optional: true },
        services: { type: Object, blackbox: true, optional: true }
    }).validator(),
    run ({ emails, password, name, phone, network, birthday }){
        let email = emails[0].address;

        if(Meteor.isServer){
            //if email and phone is unique, create the user
            let user = Accounts.createUser({
                username: phone,
                email: email,
                password: password,
                bio: {
                    name: name,
                    phone: phone,
                    network: network,
                    birthday: birthday
                }
            });;

            Roles.addUsersToRoles(user, 'airtime-user');

            //user registration is successful, send a confirmation email to user
            this.unblock();

            console.log('sending confirm email');
            console.log(confirmationEmail(emails[0].address, password, phone));

            // Email.send({
            //     to: emails[0].address,
            //     from: 'admin@poploda.com',
            //     subject: 'Successful Signup on Poploda',
            //     text: confirmationEmail(emails[0].address, password, phone)
            // });
            const mailBody = confirmationEmail(emails[0].address, password, phone);
            // Email.send({
            //     to: user.email,
            //     from: 'admin@poploda.com',
            //     subject: 'Successful Signup on Poploda',
            //     text: mailBody
            // });
        }
    }
});

export const createTransaction = new ValidatedMethod({
    name: 'transactions.create',
    validate: new SimpleSchema({
        'product_id': { type: String },
        'amount': { type: Number },
        'currency': { type: Number },
        'txn_time': { type: String },
        'txn_ref': { type: String },
        'hash': { type: String },
        'pay_item_id': { type: String },
        'network': { type: String },
        'phone': { type: String },
        'cust_id': { type: String },
        'cust_name': { type: String },
        'txn_status': { type: String },
        'isRegistered': { type: Boolean },
        'userId': { type: String }
    }).validator(),
    run (queryParams){
        //insert data into Transactions collection
        //if successful, allow routing to Interswitch page
        //if not successful, abort and return
        Transactions.insert(queryParams);
    }
});

export const updateTransactionAndCallAirVend = new ValidatedMethod({
    name: 'transactions.updateAndCallAirVend',
    validate: new SimpleSchema({
        'txnRef': { type: String },
        'payRef': { type: String },
        'retRef': { type: String },
        'cardNum': { type: String },
        'apprAmt': { type: String },
        'amt': { type: String },
    }).validator(),
    run (queryParams){
        //update Transactions collection with payRef and
        //then make a call to interswitch gateway using getTransaction
        //verify if successful and update DB with details (change status)
        //if yes, call AirVend to buy credit
        //if no, return error to user

        //retrieve the hash using the txn_ref
        //call the method to query getTransaction and
        //pass it amount, txn_ref, product_id and hash
        let txn = Transactions.findOne({txn_ref: queryParams.txnRef});
        if(txn){
            queryInterSwitchGetTransaction.call({
                product_id: txn.product_id,
                amount: txn.amount,
                txn_ref: txn.txn_ref,
                mac_key: Meteor.settings.public.MAC_KEY,
                phone: txn.phone,
                network: txn.network
            }, (error, result) => {
                if(error){
                    throw new Meteor.Error('Error', error)
                }else{
                    console.log("From methods")
                    console.log(JSON.stringify(result));

                    return result
                }
            })
        }

    }
});

export const emailBlast = new ValidatedMethod({
    name: 'admin.emailBlast',
    validate: new SimpleSchema({
        content: { type: String },
        recipients: { type: String }
    }).validator(),
    run({ content, recipients }) {
        //search through the Users collection for all users whose network matches
        // the selected recipients and store in an array
        //then get the count of the array
        //then send the email to all of them
        //then store the recipients, count, date sent and content in the EmilBlast collection
        let selector = {};
        let fields = {
            emails: 1,
            bio: 1
        }
        let receivers = [];

        if(recipients === 'ALL'){
            selector = {
                'roles': { $eq: 'airtime-user' }
            }
        }else{
            selector = {
                'roles': { $eq: 'airtime-user' },
                'bio.network': recipients
            }
        }
        receivers = Meteor.users.find(selector, fields).fetch();

        let receiver_array = [];
        if(receivers && receivers.length > 0){
            receivers.forEach((receiver) => {
                let r = {
                    email: receiver.emails[0].address,
                    phone: receiver.bio.phone
                }
                receiver_array.push(r);
                console.log('sending email to '+r.email)
            });

            EmailBlast.insert({
                content: content,
                recipients: recipients,
                recipient_count: receivers.length,
                recipient_phone_numbers: receiver_array,
                date_sent: new Date()
            });

            return 'Insert successful'
        }

        return new Meteor.Error('No records found');
    }
})

export const emailBlastRecipients = new ValidatedMethod({
    name: 'admin.emailBlast.recipients',
    validate: new SimpleSchema({
        id: { type: String },
    }).validator(),
    run({ id }) {
        let eb = EmailBlast.findOne({_id: id}, { fields: {
            recipient_phone_numbers: 1,
        }});
        return eb;
    }
})



// export const userLogin = new ValidatedMethod({
//     name: 'users.login',
//     validate: new SimpleSchema({
//         _id: { type: String, optional: true },
//         email: { type: String },
//         password: { type: String }
//     }).validator(),
//     run ({ email, password }){
//         console.log('Calling login');
//         console.log({email, password});
//
//         Meteor.loginWithPassword(email, password, function(error) {
//             if (error) {
//                 console.log("There was an error:" + error.reason);
//             } else {
//                 //FlowRouter.go('/');
//             }
//         });
//     }
// });


// export const userSignup = new ValidatedMethod({
//     name: 'users.signup',
//     validate: new SimpleSchema({
//         _id: { type: String, optional: true },
//         emails: { type: Array },
//         'emails.$': { type: Object },
//         'emails.$.address': { type: String },
//         'emails.$.verified': { type: Boolean },
//         password: { type: String },
//         name: { type: String },
//         phone: { type: String },
//         network: { type: String },
//         birthday: { type: Date, optional: true },
//         createdAt: { type: Date, optional: true },
//         services: { type: Object, blackbox: true, optional: true }
//     }).validator(),
//     run ({ emails, password, name, phone, network, birthday }){
//         let email = emails[0].address;
//         //console.log('Calling signup');
//         //console.log(email)
//
//         //console.log({email, password, name, phone});
//         // Accounts.createUser({ email, password }, (err) => {
//         //     if(err){
//         //         throw new Error(err.message)
//         //     }
//         //
//         //     return true;
//         // });
//
//         //verify the email is unique
//         // //let isEmailExisting = Accounts.findUserByEmail(email);
//         // console.log('Testing Email')
//         // let isEmailExisting = Meteor.users.findOne({}, {emails: {address: email} });
//         // console.log(isEmailExisting);
//         // if(isEmailExisting) {
//         //     console.log('Email failed, exiting')
//         //     throw new Meteor.Error('403', "Email already exists");
//         // }
//         //
//         // //verify the phone is unique
//         // console.log('Testing Phone')
//         // let isPhoneExisting = Meteor.users.find({}, {phone: phone}).fetch()[0];
//         // if(isPhoneExisting) {
//         //     return new Meteor.Error('403', "Phone number already exists");
//         // }
//         //
//         // console.log('No error in email, phone')
//         //
//         // //if email and phone is unique, create the user
//         // let _id = Accounts.createUser({ email, password });
//
//         if(Meteor.isServer){
//
//             console.log('Testing Email')
//             let isEmailExisting = Accounts.findUserByEmail(email);
//             console.log(isEmailExisting);
//             if(isEmailExisting) {
//                 console.log('Email failed, exiting')
//                 throw new Meteor.Error('403', "Email already exists");
//             }
//
//             //verify the phone is unique
//             console.log('Testing Phone')
//             let isPhoneExisting = Meteor.users.findOne({phone: phone});
//             console.log(isPhoneExisting);
//             if(isPhoneExisting) {
//                 throw new Meteor.Error('403', "Phone number already exists");
//             }
//
//             console.log('No error in email, phone')
//
//             //if email and phone is unique, create the user
//             let _id = Accounts.createUser({
//                 username: phone,
//                 email: email,
//                 password: password
//             });
//
//             Accounts.onCreateUser((options, user) => {
//                 //const { name, phone } = options;
//
//                 user.name = name;
//                 user.phone = phone;
//                 user.network = network;
//                 user.birthday = birthday;
//
//                 if( options.profile ) {
//                     user.profile = options.profile;
//                 }
//
//                 console.log(user);
//
//                 return user;
//             });
//
//             //user registration is successful, send a confirmation email to user
//             this.unblock();
//
//             console.log('sending confirm email');
//             console.log(confirmationEmail(emails[0].address, password, phone));
//
//             // Email.send({
//             //     to: emails[0].address,
//             //     from: 'admin@poploda.com',
//             //     subject: 'Successful Signup on Poploda',
//             //     text: confirmationEmail(emails[0].address, password, phone)
//             // });
//         }
//     }
// });