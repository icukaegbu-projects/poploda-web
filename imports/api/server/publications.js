import { Transactions, EmailBlast } from '../users/collections';

Meteor.publish('users.all', function () {
    return Meteor.users.find({}, {
        fields: {
            "username": 1,
            "emails": 1,
            "bio": 1,
            "createdAt": 1
        }});
});

Meteor.publish('users.one', function (id) {
    check(id, String);

    return Meteor.users.find({_id: id}, {
        fields: {
            "username": 1,
            "emails": 1,
            "bio": 1
        }});
});

Meteor.publish('transactions.all', function () {
    return Transactions.find({}, {
        fields: {
            'hash': 0,
            'currency': 0,
            'product_id': 0,
            'pay_item_id': 0,
        }})
});

Meteor.publish('transactions.user', function (phone) {
    check( phone, String );

    return Transactions.find({phone: phone}, {
        fields: {
            'hash': 0,
            'currency': 0,
            'product_id': 0,
            'pay_item_id': 0,
        }})
});

Meteor.publish('email-blast.all', function () {
    return EmailBlast.find({}, { fields: {
        recipient_phone_numbers: 0
    }, sort: {
        date_sent: 1
    }})
});

Meteor.publish('email-blast.recipients', function (id) {
    return EmailBlast.find(id, {
        recipient_phone_numbers: 1
    });
});

Meteor.publish('dashboard.registered-users', function () {
    let selector = {
        'roles': { $eq: 'airtime-user' }
    }

    return Meteor.users.find(selector);
    //create a selector for each of the networks and return an object containing them
    // let selector, mtn, airtel, glo, etisalat, total;
    //
    // selector = {
    //     'roles': { $eq: 'airtime-user' },
    //     'bio.network': 'MTN'
    // }
    // mtn = Meteor.users.find(selector).count();
    //
    // selector = {
    //     'roles': { $eq: 'airtime-user' },
    //     'bio.network': 'AIRTEL'
    // }
    // airtel = Meteor.users.find(selector).count();
    //
    // selector = {
    //     'roles': { $eq: 'airtime-user' },
    //     'bio.network': 'GLO'
    // }
    // glo = Meteor.users.find(selector).count();
    //
    // selector = {
    //     'roles': { $eq: 'airtime-user' },
    //     'bio.network': 'ETISALAT'
    // }
    // etisalat = Meteor.users.find(selector).count();
    //
    // selector = {
    //     'roles': { $eq: 'airtime-user' }
    // }
    // total = Meteor.users.find(selector).count();
    //
    // return {
    //     TOTAL: total,
    //     MTN: mtn,
    //     GLO: glo,
    //     ETISALAT: etisalat,
    //     AIRTEL: airtel
    // }
});