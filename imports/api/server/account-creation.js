/**
 * Created by ikedi on 24/09/2016.
 */
import { Accounts } from 'meteor/accounts-base';

Accounts.onCreateUser((options, user) => {
    user.bio = options.bio;

    return user;
});