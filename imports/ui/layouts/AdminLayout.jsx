import React, { Component } from 'react';
// import { Row, Col, Card, CardTitle } from 'react-materialize';
import { Layout, Content, Grid, Cell } from 'react-mdl';
import PoplodaHeader from '../components/PoplodaHeader';

class AdminLayout extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Layout fixedHeader style={{background: 'url("../images/paris-at-night-9.jpg") center / cover'}}>
                <PoplodaHeader/>
                <Content>
                    <Grid>
                        <Cell col={12} phone={4}>
                            {this.props.menu}
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={12} phone={4}>
                            {this.props.content}
                        </Cell>
                    </Grid>
                </Content>
            </Layout>

        )
    }
}

export default AdminLayout;