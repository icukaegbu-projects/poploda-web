import React, { Component } from 'react';
// import { Row, Col, Card, CardTitle } from 'react-materialize';
import { Layout, Content, Grid, Cell } from 'react-mdl';
import PoplodaHeader from '../components/PoplodaHeader';

class UserLayout extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Layout fixedHeader style={{background: 'url("../images/paris-at-night-6.jpg") center / cover'}}>
                <PoplodaHeader/>
                <Content>
                    <Grid>
                        <Cell col={6} phone={4}>
                            {this.props.menu}
                        </Cell>
                        <Cell col={6} phone={4}>
                            {this.props.content}
                        </Cell>
                    </Grid>
                </Content>
            </Layout>

        )
    }
}

export default UserLayout;