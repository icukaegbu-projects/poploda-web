import React, { Component } from 'react';
// import { Row, Col, Card, CardTitle } from 'react-materialize';
import { Layout, Content, Grid, Cell,
    Card, CardTitle, CardActions } from 'react-mdl';
import PoplodaHeader from '../components/PoplodaHeader';

class HelpLayout extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Layout fixedHeader style={{background: 'url("../images/paris-at-night-1.jpg") center / cover'}}>
                <PoplodaHeader/>
                <Content>
                    <Grid>
                        <Cell col={9} phone={4}>
                            {this.props.content}
                        </Cell>
                        <Cell col={3} phone={4}>
                            <Card
                                shadow={0}
                                style={{
                                    width: '250px',
                                    height: '550px',
                                    background: 'url(images/logo3.png) no-repeat center / contain',
                                    marginTop: '100px',
                                }}>
                                <CardTitle expand />
                            </Card>
                        </Cell>
                    </Grid>
                </Content>
            </Layout>

        )
    }
}

export default HelpLayout;