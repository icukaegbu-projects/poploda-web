import React, { Component } from 'react';
// import { Row, Col, Card, CardTitle } from 'react-materialize';
import { Layout, Content, Grid, Cell,
        Card, CardTitle, CardActions } from 'react-mdl';
import PoplodaHeader from '../components/PoplodaHeader';

class AppLayout extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Layout fixedHeader style={{background: 'url("../images/paris-at-night-1.jpg") center / cover'}}>
                <PoplodaHeader/>
                <Content>
                    <Grid>
                        <Cell col={6} phone={4}>
                            {this.props.content}
                        </Cell>
                        <Cell col={6} phone={4}>
                            <Card
                                shadow={0}
                                style={{width: '550px', height: '450px', background: 'url(images/Slide1.jpg) no-repeat center / contain', margin: 'auto'}}>
                                <CardTitle expand />
                                <CardActions
                                    style={{height: '75px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
                                    <span style={{color: '#fff', fontSize: '40px', fontWeight: '700', margin: 'auto'}}>
                                        Welcome to Poploda
                                    </span>
                                </CardActions>
                            </Card>
                        </Cell>
                    </Grid>
                </Content>
            </Layout>

        )
    }
}

export default AppLayout;


{/*<div className="container-bs">*/}
    {/*<Row className="reset">*/}
        {/*<Col s={12} m={12}>*/}
            {/*<PoploadaHeader />*/}
        {/*</Col>*/}
    {/*</Row>*/}

    {/*<Row>*/}
        {/*<Col s={12} m={6} l={6}>*/}
            {/*{this.props.content}*/}
        {/*</Col>*/}
        {/*<Col s={12} m={6} l={6}>*/}
            {/*<Card*/}
                {/*header={*/}
                    {/*<CardTitle*/}
                        {/*reveal*/}
                        {/*image='images/Slide1.jpg'*/}
                        {/*waves='light'*/}
                    {/*/>*/}
                {/*}*/}
            {/*>*/}
                {/*<h2 className="center-align">Welcome to Poploda</h2>*/}
            {/*</Card>*/}
        {/*</Col>*/}
    {/*</Row>*/}
{/*</div>*/}