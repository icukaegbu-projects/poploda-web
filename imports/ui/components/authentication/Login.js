/**
 * Created by ikedi on 02/07/2016.
 */
import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
//import { Bert } from 'meteor/themeteorchef:bert';
import LoginForm from './LoginForm';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                email: '',
                password: '',
                loggingIn: false
            }
        }

        this.loginUser = this.loginUser.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    handleInputChange(evt){
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields })
    }

    resetState(){
        const fields = this.state.fields;

        fields.email = '';
        fields.password = '';
        this.setState({ fields });
    }

    loginUser(e) {
        e.preventDefault();
        let { email, password } = this.state.fields;

        Meteor.loginWithPassword(email, password, function(error) {
            if (error) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'Login Error')
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Login Successful', 'Success');

                FlowRouter.go('user-home');
            }
        });
    }

    render() {
        return (
            <LoginForm
                email={this.state.fields.email}
                password={this.state.fields.password}
                loggingIn={this.state.fields.loggingIn}
                loginUser={this.loginUser}
                handleInputChange={this.handleInputChange}
                resetState={this.resetState}
            />
        )
    }
}

export default Login;