/**
 * Created by ikedi on 11/08/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardText,
    CardMenu, CardActions,
    IconButton, Icon, Button, Grid, Cell,
    Textfield, Spinner } from 'react-mdl';

const checkProps =(props) => (
    props.email === '' || props.password === ''
)

const checkScreenWidth = () => (
    screen.width >= 720 ? '475px' : '300px'
)

const LoginForm = (props) => {
    return (
        <form>
            <Card shadow={0} style={{minWidth: '300px', width: checkScreenWidth()}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '75px',
                        background: '#9940A9',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >

                    {
                       props.loggingIn ?
                            <Grid>
                                <Cell col={4} offset={5}>
                                    <Spinner flashing size="small"/>
                                </Cell>
                            </Grid>
                            : <h4>Log In</h4>
                    }
                </CardTitle>
                <CardText style={{paddingRight: '0px'}}>
                    <Grid>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                name="email"
                                value={props.email}
                                label="Email or Phone Number"
                                type="email"
                                onChange={props.handleInputChange}>
                            </Textfield>
                        </Cell>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                name="password"
                                value={props.password}
                                label="Password"
                                type="password"
                                onChange={props.handleInputChange}>
                            </Textfield>
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={4} offsetDesktop={2}><a href="#">Forgot Password</a></Cell>
                        <Cell col={4} offsetDesktop={1}><a href="/signup">New User? Register</a></Cell>
                    </Grid>
                </CardText>
                <CardActions border>
                    <Grid style={{margin: '0px', padding: '0px'}}>
                        <Cell col={3} offsetDesktop={2}>
                            <Button
                                raised colored ripple
                                onClick={props.loginUser}
                                disabled={checkProps(props)}
                            >
                                Login
                            </Button>
                        </Cell>
                        <Cell col={3} offsetDesktop={2}>
                            <Button
                                raised accent ripple
                                onClick={props.resetState}
                                type="reset">
                                Cancel
                            </Button>
                        </Cell>
                    </Grid>
                </CardActions>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/" />
                </CardMenu>
            </Card>
        </form>
    )
};

LoginForm.propTypes = {
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    loggingIn: PropTypes.bool,
    loginUser: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    resetState: PropTypes.func.isRequired
};

export default LoginForm;

{/*<Card shadow={0} style={{width: '512px'}}>*/}