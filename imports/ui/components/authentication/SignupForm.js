/**
 * Created by ikedi on 19/07/2016.
 */
import React, { PropTypes, Component } from 'react';
import { Card, CardTitle, CardText,
    CardMenu, CardActions,
    IconButton, Icon, Button, Grid, Cell,
    Textfield, Spinner } from 'react-mdl';

const checkProps = (props) => (
    props.email === '' || props.name === '' || props.phone === '' || props.password === '' || props.confirmPassword === ''
)

const checkScreenWidth = () => (
    screen.width >= 720 ? '475px' : '300px'
)
const SignupForm = (props) => {
    return (
        <form>
            <Card shadow={0} style={{minWidth: '300px', width: checkScreenWidth()}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '75px',
                        background: '#FFB300',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    {
                        props.signup ?
                            <Grid style={{margin: '0px'}}>
                                <Cell col={4} offset={1}>
                                    <Spinner flashing/>
                                </Cell>
                            </Grid>
                            : <h4>Sign Up</h4>
                    }
                    {/*{*/}
                        {/*!this.props.errorMessage && this.props.signup ?*/}
                            {/*<Row style={{margin: '0px'}}>*/}
                                {/*<Col s={4} offset="s5">*/}
                                    {/*<Spinner flashing/>*/}
                                {/*</Col>*/}
                            {/*</Row>*/}
                            {/*: <h4>Sign Up</h4>*/}
                    {/*}*/}
                    {/*{*/}
                        {/*this.props.errorMessage !== '' ?*/}
                            {/*Bert.alert({*/}
                                {/*title: 'Error',*/}
                                {/*message: this.props.errorMessage,*/}
                                {/*type: 'danger',*/}
                                {/*style: 'growl-top-left'*/}
                            {/*}) */}
                            {/*: ''*/}
                    {/*}*/}
                </CardTitle>
                <CardText style={{paddingRight: '0px'}}>
                    <Grid>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Email"
                                pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"
                                error="Email must contain @ symbol and a domain name"
                                type="email"
                                value={props.email}
                                onChange={props.handleInputChange}
                                name="email"
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Name"
                                pattern="^[A-Za-z0-9 ]{3,20}$"
                                error="Name must be at least 3 letters"
                                type="text"
                                value={props.name}
                                onChange={props.handleInputChange}
                                name="name"
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Phone"
                                pattern="^\d{11}$"
                                error="Phone must be 11 digits"
                                type="tel"
                                value={props.phone}
                                onChange={props.handleInputChange}
                                name="phone"
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={4} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Network (All CAPS)"
                                pattern="^(AIRTEL|ETISALAT|GLO|MTN)$"
                                error="Network must be one of AIRTEL/ETISALAT/GLO/MTN"
                                type="text"
                                value={props.network}
                                onChange={props.handleInputChange}
                                name="network"
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={4} offsetDesktop={1}>
                            <Textfield
                                floatingLabel
                                label=""
                                pattern=""
                                error=""
                                type="date"
                                value={props.birthday}
                                onChange={props.handleInputChange}
                                name="birthday"
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={4} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Password"
                                pattern="^[A-Za-z0-9!@#$%^&*()_]{6,20}$"
                                error="Password not less than 6 and not more than 20"
                                type="password"
                                value={props.password}
                                onChange={props.handleInputChange}
                                name="password"
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={4} offsetDesktop={1}>
                            <Textfield
                                floatingLabel
                                label="Confirm Password"
                                pattern="^[A-Za-z0-9!@#$%^&*()_]{6,20}$"
                                error="Password not less than 6 and not more than 20"
                                type="password"
                                value={props.confirmPassword}
                                onChange={props.handleInputChange}
                                name="confirmPassword"
                            >
                            </Textfield>
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={6} offsetDesktop={2}><a href="/login">Already Registered? Login</a></Cell>
                    </Grid>
                </CardText>


                <CardActions border>
                    <Grid style={{margin: '0px', padding: '0px'}}>
                        <Cell col={3} offsetDesktop={2}>
                            <Button
                                raised colored ripple
                                type="submit"
                                onClick={props.handleSignup}
                                disabled={checkProps(props)}
                            >
                                Sign-Up
                            </Button>
                        </Cell>
                        <Cell col={3} offsetDesktop={2}>
                            <Button
                                raised accent ripple
                                onClick={props.resetState}
                                type="reset">
                                Cancel
                            </Button>
                        </Cell>
                    </Grid>
                </CardActions>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/" />
                </CardMenu>
            </Card>
        </form>
    )
}

SignupForm.propTypes = {
    // toggle: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    resetState: PropTypes.func.isRequired,
    handleSignup: PropTypes.func.isRequired,

    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    confirmPassword: PropTypes.string.isRequired,
    name: PropTypes.string,
    phone: PropTypes.string,
    signup: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string,
    network: PropTypes.string,
    birthday: PropTypes.date,
}

export default SignupForm;

{/*<Card shadow={0} style={{width: '512px'}}>*/}