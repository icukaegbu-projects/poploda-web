/**
 * Created by ikedi on 02/07/2016.
 */
/**
 * Created by ikedi on 02/07/2016.
 */
import React, { Component, PropTypes } from 'react';
import SignupForm from './SignupForm';
import { userSignup } from '../../../api/users/methods';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';

import regex_validations from '../../../api/regex_validations';

class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                email: '',
                password: '',
                confirmPassword: '',
                name: '',
                phone: '',
                network: '',
                birthday: '',
                signup: false,
                errorMessage: ''
            }
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSignup = this.handleSignup.bind(this);
        this.clearErrorAndSignup = this.clearErrorAndSignup.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    handleSignup(e){
        e.preventDefault();
        //console.log('In handleSignup')

        //lead preloader
        this.setState({signup: true});

        let { email, password, name, phone, network, birthday } = this.state.fields;

        //validate the form
        let isFormValid = this.validateForm();
        //console.log('called validateForm: '+isFormValid);

        //call signup method
        if(isFormValid){
            let emails = [];
            let user_email = { address: email, verified: false }
            emails.push(user_email)

            userSignup.call({emails, password, name, phone, network, birthday},  (message) => {
            //Meteor.call('users.signup',{emails, password, name, phone},  (message) => {
                if (message) {
                    if(message.reason === 'Email already exists' || message.reason === 'Phone number already exists'){
                        this.resetState();
                    }

                    toastr.options = {"positionClass":'toast-top-left'};
                    toastr.error(message.reason, 'Signup Error');

                    return;
                } else {
                    Meteor.loginWithPassword(email, password, function(error) {
                        if (error) {
                            toastr.options = {"positionClass":'toast-top-left'};
                            toastr.error(error.reason, 'Login Error')
                        } else {
                            toastr.options = {"positionClass":'toast-top-left'};
                            toastr.success('Login Successful', 'Success');

                            FlowRouter.go('user-home');
                        }
                    });
                }
            });
        }

        //if signup successful, route to user dashboard page
        //display username and avatar at header
    }

    validateForm(){
        //if form validation fails, set errorMessage to the errors and display them
        let ck_name = regex_validations.ck_name;
        let ck_email = regex_validations.ck_email;
        let ck_password =  regex_validations.ck_password;
        let ck_phone = regex_validations.ck_phone; //validate that phone is 11 digits

        let { email, password, confirmPassword, name, phone } = this.state.fields;
        let errorMessage = '';

        if ( email === '' || password === '') {
            errorMessage += 'Email and/or password cannot be null.\n';
            this.setState({
                email: '',
                password: '',
                errorMessage: errorMessage
            });
            //console.log(errorMessage)
            return false;
        }

        if ( password !== confirmPassword ) {
            errorMessage += 'Passwords do not match';
            //console.log(errorMessage)
            this.setState({
                password: '',
                confirmPassword: '',
                errorMessage: errorMessage
            });
            return false;
        }

        //if above validations passed,
        // clear errorMessage before checking regular expressions
        this.clearErrorAndSignup();

        //validate regular expressions
        if (!ck_name.test(name)) {
            errorMessage += "Please enter a valid Name.\n";
        }
        if (!ck_email.test(email)) {
            errorMessage += "Please enter a valid Email Address.\n";
        }
        if (!ck_password.test(password)) {
            errorMessage += "Please enter a valid Password.\n";
        }
        if (!ck_phone.test(phone)) {
            errorMessage += "Please enter a valid Phone Number.\n";
        }

        //if errorMessage is not null, clear the state and return
        if(errorMessage !== '' ){
            this.setState({
                email: '',
                password: '',
                name: '',
                phone: ''
            });
            return false;
        }

        //set this.state.errorMessage to errorMessage
        this.setState({errorMessage: errorMessage.toString()});

        return true;
    }

    clearErrorAndSignup(){
        this.setState({errorMessage: '', signup: false});
    }

    handleInputChange(evt)
    {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields });
    }

    resetState() {
        const fields = {
            email: '',
            password: '',
            confirmPassword: '',
            name: '',
            phone: '',
            network: '',
            birthday: '',
            signup: false,
            errorMessage: ''
        }

        this.setState({ fields });
    }

    render() {
        return (
            <div>
                <SignupForm
                    handleInputChange={this.handleInputChange}
                    handleSignup={this.handleSignup}
                    resetState={this.resetState}
                    email={this.state.fields.email}
                    password={this.state.fields.password}
                    confirmPassword={this.state.fields.confirmPassword}
                    name={this.state.fields.name}
                    phone={this.state.fields.phone}
                    signup={this.state.fields.signup}
                    network={this.state.fields.network}
                    birthday={this.state.fields.birthday}
                    errorMessage={this.state.fields.errorMessage}
                />
            </div>
        )
    }
}

export default Signup;
