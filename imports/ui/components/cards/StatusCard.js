/**
 * Created by ikedi on 13/07/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Card, CardActions, CardTitle, CardText } from 'react-mdl';

class StatusCard extends Component {
    constructor(props) {
        super(props);

        this.checkSize = this.checkSize.bind(this);
    }

    checkSize(size){
        if(size === 'small' || size === ''){
            return {
                height: '150px',
                width: '150px',
                background: `${this.props.color}`,
                paddingBottom: '10px',
                paddingLeft: '10px',
                paddingTop: '0px'
            }
        }else if(size === 'tiny'){
            return {
                height: '75px',
                width: '75px',
                background: `${this.props.color}`,
                paddingBottom: '0px',
                paddingLeft: '0px',
                paddingTop: '0px'
            }
        }else{
            return {
                height: '250px',
                width: '250px',
                background: `${this.props.color}`,
                paddingBottom: '20px',
                paddingLeft: '20px',
                paddingTop: '0px'
            }
        }
    }

    render(){
        return (
            <Card
                shadow={0}
                style={this.checkSize(this.props.size)}
            >

                <CardTitle
                    expand
                    style={{
                        alignItems: 'flex-start',
                        color: '#fff'
                    }}>
                    <h4
                        style={{
                            marginBottom: '10px',
                            fontSize: this.props.headerSize ? `${this.props.headerSize}px` :'20px',
                            fontFamily: 'Arial',
                            lineHeight: '20px' /*was not included before */
                        }}>
                        {this.props.title}
                    </h4>
                </CardTitle>
                <CardText
                    style={{
                        color: this.props.contentTextColor ? `${this.props.contentTextColor}` : '#fff',
                    }}
                >
                    {this.props.content}
                </CardText>
            </Card>
        )
    }
}

StatusCard.propTypes = {
    size: PropTypes.oneOf(['small', 'large']),
    color: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    contentTextColor: PropTypes.string,
    headerSize: PropTypes.string,
}

export default StatusCard;
