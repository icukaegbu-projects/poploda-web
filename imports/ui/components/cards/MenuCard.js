/**
 * Created by ikedi on 14/07/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Card, CardActions, CardTitle,
        Icon, FABButton, Button } from 'react-mdl';

class MenuCard extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
                <Card
                    shadow={0}
                    style={{
                        height: '150px',
                        width: '150px',
                        background: `${this.props.color}`,
                        paddingBottom: '20px',
                        paddingLeft: '20px',
                        paddingTop: '0px'
                    }}>

                    <CardTitle
                        expand
                        style={{
                            alignItems: 'flex-start'
                        }}>
                        <h4
                            style={{
                                marginBottom: '10px',
                                fontSize: '20px',
                                color: 'white',
                                fontFamily: 'Arial'
                            }}>
                            {this.props.title}
                        </h4>
                    </CardTitle>
                    <CardActions >
                        <FABButton
                            ripple
                            onClick={this.props.onClick}
                            className={this.props.color}
                            style={{marginBottom: '-10px', color: this.props.color }}
                        >
                            <Icon name={this.props.icon} style={{color: 'white'}}></Icon>
                        </FABButton>
                    </CardActions>

                </Card>
            </div>

        )
    }
}

MenuCard.propTypes = {
    onClick: PropTypes.func.isRequired
}

export default MenuCard;

{/*<Button*/}
    {/*onClick={this.props.onClick}*/}
    {/*waves='light'*/}
    {/*className="btn-flat btn-large white-text"*/}
    {/*style={{marginBottom: '-10px'}}>*/}
    {/*{this.props.title}*/}
{/*</Button>*/}