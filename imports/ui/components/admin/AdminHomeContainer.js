/**
 * Created by ikedi on 05/10/2016.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import AdminHomeForm from './AdminHomeForm';
import { Transactions, EmailBlast } from '../../../api/users/collections';

export default AdminHomeContainer = createContainer(({ params }) => {
    const registeredUsersHandle = Meteor.subscribe('dashboard.registered-users');
    const registeredUserLoading = !registeredUsersHandle.ready();
    const registeredUsers = getRegisteredUsers();

    return {
        registeredUserLoading,
        registeredUsers
    };
}, AdminHomeForm);


function getRegisteredUsers() {
    let selector, mtn, airtel, glo, etisalat, total;

    selector = {
        'bio.network': 'MTN'
    }
    mtn = Meteor.users.find(selector).count();

    selector = {
        'bio.network': 'AIRTEL'
    }
    airtel = Meteor.users.find(selector).count();

    selector = {
        'bio.network': 'GLO'
    }
    glo = Meteor.users.find(selector).count();

    selector = {
        'bio.network': 'ETISALAT'
    }
    etisalat = Meteor.users.find(selector).count();

    selector = {
        'roles': { $eq: 'airtime-user' }
    }
    total = Meteor.users.find(selector).count();

    return {
        TOTAL: total,
        MTN: mtn,
        GLO: glo,
        ETISALAT: etisalat,
        AIRTEL: airtel
    }
}