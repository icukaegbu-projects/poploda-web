/**
 * Created by ikedi on 21/09/2016.
 */
import React, { Component } from 'react';
import { Grid, Cell, DataTable, TableHeader, IconButton,
    Card, CardText, CardTitle, CardMenu, Button,
    Dialog, DialogActions, DialogContent, DialogTitle } from 'react-mdl';

class WalletTopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isViewing: false
        }

        this.createButton = this.createButton.bind(this);
        this.showDialog = this.showDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.viewTransaction = this.viewTransaction.bind(this);
    }

    showDialog() {
        return (
            <Dialog open={this.state.isViewing}>
                <DialogTitle>Allow data collection?</DialogTitle>
                <DialogContent>
                    <p>Allowing us to collect data will let us get you the information you want faster.</p>
                </DialogContent>
                <DialogActions>
                    <Button type='button'>Agree</Button>
                    <Button type='button' onClick={this.closeDialog}>Disagree</Button>
                </DialogActions>
            </Dialog>
        );
    }

    closeDialog() {
        this.setState({
            isViewing: false
        })
    }

    viewTransaction(id) {
        console.log('viewing')
        this.setState({
            isViewing: true
        });
        //load the data of the passed in ID and display in dialog
    }

    createButton(id) {
        return (
            <div>
                <IconButton
                    name="zoom_in"
                    onClick={this.viewTransaction.bind(null, id)}
                    style={{
                        color: '#1E88E5'
                    }}
                />
            </div>
        )
    }

    render() {
        return (
            <Card shadow={0} style={{minWidth: '960px'}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '55px',
                        background: '#2962FF',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>Wallet Top-up</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#424242',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    {this.showDialog()}
                    <Grid>
                        <Cell col={12}>
                            <DataTable
                                shadow={0}
                                selectable
                                rows={[
                                    {email: 'Acrylic (Transparent)', phone: '08022932368', name: 'Chike', actions: this.createButton('123')},
                                    {email: 'Plywood (Birch)', phone: '08055968687', name: 'Chibueze', actions: this.createButton('456')},
                                    {email: 'Laminate (Gold on Blue)', phone: '08140110691', name: 'Chidu', actions: this.createButton('789')}
                                ]}
                                style={{
                                    minWidth: '900px'
                                }}
                            >
                                <TableHeader name="phone" tooltip="Phone Number" style={{width:'100px'}}>Phone</TableHeader>
                                <TableHeader name="email" tooltip="Email" style={{width:'200px'}}>Email</TableHeader>
                                <TableHeader name="network" tooltip="Network" style={{width:'50px'}}>Network</TableHeader>
                                <TableHeader name="amount" tooltip="Amount" style={{width:'50px'}}>Amount</TableHeader>
                                <TableHeader name="status" tooltip="Status" style={{width:'100px'}}>T/Status</TableHeader>
                                <TableHeader name="transactionDate" tooltip="Transaction Date" style={{width:'100px'}}>T/Date</TableHeader>
                                <TableHeader name="actions" tooltip="actions">Actions</TableHeader>
                            </DataTable>
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/admin" />
                </CardMenu>
            </Card>
        );
    }
}

export default WalletTopup;