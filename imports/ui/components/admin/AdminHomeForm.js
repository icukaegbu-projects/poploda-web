/**
 * Created by ikedi on 04/10/2016.
 */

import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardText, CardActions,
         Icon, Button, Grid, Cell } from 'react-mdl';
//import StatusCard from '../cards/StatusCard';

class AdminHomeForm extends Component {
    constructor(props){
        super(props);

        this.renderReport = this.renderReport.bind(this);
        this.topTenUsers = this.topTenUsers.bind(this);
        this.registeredUsers = this.registeredUsers.bind(this);
        this.dailySales = this.dailySales.bind(this);
        this.networkPerformance = this.networkPerformance.bind(this);
    }

    topTenUsers() {
        // return (
        //     this.renderReport('Top Ten Users', '#334543', 'Top ten users')
        // )
        return (
            <Card shadow={0} style={{width: '256px', height: '256px', background: '#453321'}}>
                <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
                    <h4 style={{marginTop: '0'}}>
                        Top Ten Users<br />
                    </h4>
                </CardTitle>
                <CardText
                    style={{
                        color: 'white',
                        fontSize: '13px',
                        fontWeight: 'bold'
                    }}
                >
                    <table>
                        <tr>
                            <th>#</th>
                            <th>Network</th>
                            <th>Amount</th>
                        </tr>
                        <tbody>
                            <tr>
                                <td>08022932368</td>
                                <td>AIRTEL</td>
                                <td>3,500</td>
                            </tr>
                            <tr>
                                <td>08140110691</td>
                                <td>MTN</td>
                                <td>3,300</td>
                            </tr>
                            <tr>
                                <td>08035815480</td>
                                <td>MTN</td>
                                <td>2,900</td>
                            </tr>
                            <tr>
                                <td>08057179705</td>
                                <td>GLO</td>
                                <td>2,750</td>
                            </tr>
                        </tbody>
                    </table>
                </CardText>
                <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
                    <Button colored style={{color: '#fff'}}>Total: #3</Button>
                    <div className="mdl-layout-spacer"></div>
                    <Icon name="event" />
                </CardActions>
            </Card>
        )
    }

    registeredUsers() {
        // return (
        //     this.renderReport('Registered Users', '#334543', 'Top ten users')
        // )
        return (
            <Card shadow={0} style={{width: '256px', height: '256px', background: '#2235A4'}}>
                <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
                    <h4 style={{marginTop: '0'}}>
                        Registered Users<br />
                    </h4>
                </CardTitle>
                <CardText
                    style={{
                        color: 'white',
                        fontSize: '35px',
                        fontWeight: 'bold'
                    }}
                >
                    <ul>
                        <li>AIRTEL: {this.props.registeredUsers.AIRTEL}</li>
                        <li>ETISALAT: {this.props.registeredUsers.ETISALAT}</li>
                        <li>GLO: {this.props.registeredUsers.GLO}</li>
                        <li>MTN: {this.props.registeredUsers.MTN}</li>
                    </ul>
                </CardText>
                <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
                    <Button colored style={{color: '#fff'}}>Total: {this.props.registeredUsers.TOTAL}</Button>
                    <div className="mdl-layout-spacer"></div>
                    <Icon name="event" />
                </CardActions>
            </Card>
        )
    }

    dailySales() {
        // return (
        //     this.renderReport('Daily Sales', '#334543', 'Daily Sales')
        // )
        return (
            <Card shadow={0} style={{width: '256px', height: '256px', background: '#9AE64D'}}>
                <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
                    <h4 style={{marginTop: '0'}}>
                        Daily Sales<br />
                    </h4>
                </CardTitle>
                <CardText
                    style={{
                        color: 'white',
                        fontSize: '35px',
                        fontWeight: 'bold'
                    }}
                >
                    <ul>
                        <li>MTN: 20</li>
                        <li>AIRTEL: 100</li>
                        <li>GLO: 33</li>
                        <li>ETISALAT: 5</li>
                    </ul>
                </CardText>
                <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
                    <Button colored style={{color: '#fff'}}>Total: #3</Button>
                    <div className="mdl-layout-spacer"></div>
                    <Icon name="event" />
                </CardActions>
            </Card>
        )
    }

    networkPerformance() {
        return (
            this.renderReport('Network Performance', '#334543', 'NetPerf')
        )
    }

    renderReport(title, color, data) {
        return (
            <Card shadow={0} style={{width: '256px', height: '256px', background: color}}>
                <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
                    <h4 style={{marginTop: '0'}}>
                        {title}<br />
                    </h4>
                </CardTitle>
                <CardText
                    style={{
                        color: 'white',
                        fontSize: '20px',
                        fontWeight: 'bold'
                    }}
                >
                    {data}
                </CardText>
                <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
                    <Button colored style={{color: '#fff'}}>Add to Calendar</Button>
                    <div className="mdl-layout-spacer"></div>
                    <Icon name="event" />
                </CardActions>
            </Card>
        )
    }

    render(){
        return (
            <Grid>
                <Cell col={3} tablet={6} phone={12}>
                    {this.topTenUsers()}
                </Cell>
                <Cell col={3} tablet={6} phone={12}>
                    {this.registeredUsers()}
                </Cell>
                <Cell col={3} tablet={6} phone={12}>
                    {this.dailySales()}
                </Cell>
                <Cell col={3} tablet={6} phone={12}>
                    {this.networkPerformance()}
                </Cell>
            </Grid>
        )
    }
}

AdminHomeForm.propTypes = {
    registeredUserLoading: PropTypes.bool,
    registeredUsers: PropTypes.object.isRequired
}

export default AdminHomeForm;