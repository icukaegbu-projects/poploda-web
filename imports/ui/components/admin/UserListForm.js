/**
 * Created by ikedi on 21/09/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Grid, Cell, DataTable, TableHeader, IconButton,
    Card, CardText, CardTitle, CardMenu, Button,
    Dialog, DialogActions, DialogContent, DialogTitle } from 'react-mdl';
import { userDelete } from '../../../api/users/methods';

class UsersList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // isEditing: false,
            isDeleting: false,
            user: null
        }

        this.createButton = this.createButton.bind(this);
        // this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.showDialog = this.showDialog.bind(this);
        this.showDeleteDialog = this.showDeleteDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.renderUsers = this.renderUsers.bind(this);
    }

    showDialog() {
        let username = this.state.user ? this.state.user.username : '';
        return (
            <Dialog open={this.state.isDeleting}>
                <DialogTitle>Confirm Delete?</DialogTitle>
                <DialogContent>
                    <p>Delete user: {username}?</p>
                </DialogContent>
                <DialogActions>
                    <Button type='button' onClick={this.deleteUser.bind(null, this.state.user)}>Confirm</Button>
                    <Button type='button' onClick={this.closeDialog}>Cancel</Button>
                </DialogActions>
            </Dialog>
        );
    }

    closeDialog() {
        this.setState({
            // isEditing: false,
            isDeleting: false,
            user: null
        })
    }

    showDeleteDialog(user) {
        this.setState({
            isDeleting: true,
            user: user
        });
    }

    deleteUser(user) {
        //load the data of the passed in ID and display in dialog
        userDelete.call({ _id: user._id }, (error, response) => {
            if (error) {
                console.log(error);
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error, 'User Delete Error');
            } else {
                console.log('User Delete Successful!');
                console.log(response);

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('User Deleted Successfully.', 'Success');
            }
        });

        this.closeDialog();
    }

    createButton(user) {
        return (
            <div>
                <IconButton
                    name="delete"
                    onClick={this.showDeleteDialog.bind(null, user)}
                    style={{
                        color: '#B71C1C'
                    }}
                />
                {/*<IconButton*/}
                    {/*name="edit"*/}
                    {/*onClick={this.editUser.bind(null, user)}*/}
                    {/*style={{*/}
                        {/*color: '#1E88E5'*/}
                    {/*}}*/}
                {/*/>*/}
            </div>
        )
    }

    renderUsers() {
        if (this.props.loading) {
            return [];
        }else{
            return this.props.users.map((user) => {
                return  {
                    email: user.emails[0].address,
                    phone: user.username,
                    network: user.bio.network,
                    name: user.bio.name,
                    dateRegistered: user.createdAt.toDateString(),
                    actions: this.createButton(user)
                }
            });
        }

    }

    render() {
        return (
            <Card shadow={0} style={{minWidth: '900px'}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '55px',
                        background: '#5E35B1',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>Users List</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#424242',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    {this.showDialog()}
                    <Grid>
                        <Cell col={12}>
                            <DataTable
                                shadow={0}
                                selectable
                                rows={this.renderUsers()}
                                style={{
                                    minWidth: '800px'
                                }}
                            >
                                <TableHeader name="phone" tooltip="Phone Number" style={{width:'100px'}}>Phone</TableHeader>
                                <TableHeader name="network" tooltip="Network" style={{width:'30px'}}>Network</TableHeader>
                                <TableHeader name="email" tooltip="Email" style={{width:'200px'}}>Email</TableHeader>
                                <TableHeader name="name" tooltip="Name" style={{width:'200px'}}>Name</TableHeader>
                                <TableHeader name="dateRegistered" tooltip="Date Registered" style={{width:'100px'}}>Date Registered</TableHeader>
                                <TableHeader name="actions" tooltip="actions">Actions</TableHeader>
                            </DataTable>
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/admin" />
                </CardMenu>
            </Card>
        );
    }
}

UsersList.propTypes = {
    users: PropTypes.array.isRequired,
    loading: PropTypes.bool
}

export default UsersList;