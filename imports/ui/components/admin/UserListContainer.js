/**
 * Created by ikedi on 23/09/2016.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import UserListForm from './UserListForm';

export default UserListContainer = createContainer(({ params }) => {
    const usersHandle = Meteor.subscribe('users.all');
    const loading = !usersHandle.ready();
    const users = Meteor.users.find().fetch();
    return {
        loading,
        users
    };
}, UserListForm);