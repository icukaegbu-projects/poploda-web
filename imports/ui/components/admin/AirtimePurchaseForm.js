/**
 * Created by ikedi on 21/09/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Grid, Cell, DataTable, TableHeader, IconButton,
    Card, CardText, CardTitle, CardMenu, Button,
    Dialog, DialogActions, DialogContent, DialogTitle } from 'react-mdl';

class AirtimePurchase extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isViewing: false,
            ap: null
        }

        this.createButton = this.createButton.bind(this);
        this.showDialog = this.showDialog.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.viewTransaction = this.viewTransaction.bind(this);
        this.renderAirtimePurchase = this.renderAirtimePurchase.bind(this);
    }

    showDialog() {
        let ap = this.state.ap ? this.state.ap : '';
        return (
            <Dialog open={this.state.isViewing}>
                <DialogTitle style={{fontSize: '25px'}}>Transaction#: {ap.txn_ref}</DialogTitle>
                <DialogContent>
                    <span>Customer: {ap.cust_name}</span><br/>
                    <span>Customer #: {ap.cust_id}</span><br/>
                    <span>Registered?: {ap.isRegistered}</span><br/>
                    <span>Status: {ap.txn_status}</span><br/>
                    <span>Network: {ap.network}</span><br/>
                    <span>Amount: {ap.amount}</span><br/>
                    <span>Time: {ap.txn_time}</span><br/>
                </DialogContent>
                <DialogActions>
                    <Button type='button' onClick={this.closeDialog}>OK</Button>
                </DialogActions>
            </Dialog>
        );
    }

    closeDialog() {
        this.setState({
            isViewing: false,
            ap: null
        })
    }

    viewTransaction(ap) {
        this.setState({
            isViewing: true,
            ap: ap
        });
        //load the data of the passed in ID and display in dialog
    }

    createButton(ap) {
        return (
            <div>
                <IconButton
                    name="zoom_in"
                    onClick={this.viewTransaction.bind(null, ap)}
                    style={{
                        color: '#1E88E5'
                    }}
                />
            </div>
        )
    }

    renderAirtimePurchase() {
        if (this.props.loading) {
            return [];
        }else {
            return this.props.airtimePurchase.map((ap) => {
                return {
                    email: ap.cust_name,
                    phone: ap.phone,
                    network: ap.network,
                    txnRef: ap.txn_ref,
                    amount: ap.amount / 100,
                    status: ap.txn_status,
                    transactionDate: ap.txn_time,
                    actions: this.createButton(ap)
                }
            });
        }
    }

    render() {
        return (
            <Card shadow={0} style={{minWidth: '960px'}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '55px',
                        background: '#FFB300',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>Airtime Purchases</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#424242',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    {this.showDialog()}
                    <Grid>
                        <Cell col={12}>
                            <DataTable
                                shadow={0}
                                selectable
                                rows={this.renderAirtimePurchase()}
                                style={{
                                    minWidth: '800px'
                                }}
                            >
                                <TableHeader name="phone" tooltip="Phone Number" style={{width:'80px'}}>Phone</TableHeader>
                                <TableHeader name="email" tooltip="Email" style={{width:'200px'}}>Email</TableHeader>
                                <TableHeader name="network" tooltip="Network" style={{width:'50px'}}>Network</TableHeader>
                                <TableHeader name="amount" tooltip="Amount" style={{width:'50px'}}>Amount</TableHeader>
                                <TableHeader name="status" tooltip="Status" style={{width:'100px'}}>T/Status</TableHeader>
                                <TableHeader name="transactionDate" tooltip="Transaction Date" style={{width:'80px'}}>T/Date</TableHeader>
                                <TableHeader name="actions" tooltip="actions">Actions</TableHeader>
                            </DataTable>
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/admin" />
                </CardMenu>
            </Card>
        );
    }
}

AirtimePurchase.propTypes = {
    airtimePurchase: PropTypes.array.isRequired,
    loading: PropTypes.bool
}

export default AirtimePurchase;