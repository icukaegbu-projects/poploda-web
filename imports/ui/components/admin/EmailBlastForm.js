/**
 * Created by ikedi on 26/09/2016.
 */

import React, { Component, PropTypes } from 'react';
import { Grid, Cell, DataTable, TableHeader, IconButton, Textfield,
    Card, CardText, CardTitle, CardMenu, Button, Icon, Radio, RadioGroup,
    Dialog, DialogActions, DialogContent, DialogTitle } from 'react-mdl';
import { emailBlast, emailBlastRecipients } from '../../../api/users/methods';

class EmailBlast extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // isEditing: false,
            isDeleting: false,
            blast: null,
            fields: {
                content: '',
                network: ''
            }
        }

        this.createButton = this.createButton.bind(this);
        this.showDialog = this.showDialog.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.createRadioButton = this.createRadioButton.bind(this);
        this.renderEmailBlasts = this.renderEmailBlasts.bind(this);
        this.checkProps = this.checkProps.bind(this);
        this.sendEmailBlast = this.sendEmailBlast.bind(this);
        this.viewEmailBlast = this.viewEmailBlast.bind(this);
    }

    showDialog() {
        let ap = this.state.blast ? this.state.blast : '';
        return (
            <Dialog open={this.state.isViewing}>
                <DialogTitle style={{fontSize: '25px'}}>Recipients#: {ap.recipients}</DialogTitle>
                <DialogContent>
                    <ul>
                        {
                            ap.recipient_phone_numbers ?
                                ap.recipient_phone_numbers.map((r) => {
                                    return <li key={r.phone}>{r.phone} - {r.email}</li>
                                })
                             : ''
                        }
                    </ul>
                </DialogContent>
                <DialogActions>
                    <Button type='button' onClick={this.closeDialog}>OK</Button>
                </DialogActions>
            </Dialog>
        );
    }

    closeDialog() {
        this.setState({
            isViewing: false,
            blast: null
        })
    }

    viewEmailBlast(blast) {
        this.setState({
            isViewing: true,
            blast
        });
        //load the data of the passed in ID and display in dialog
        emailBlastRecipients.call({id: blast._id}, (error, response) => {
            if (error) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error, 'Error retrieving Recipients data');
            } else {
                blast.recipient_phone_numbers = response.recipient_phone_numbers;
                this.setState({
                    blast
                });
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Recipients data retrieved successfully.', 'Success');
            }
        });
    }

    createButton(blast) {
        return (
            <div>
                <IconButton
                    name="zoom_in"
                    onClick={this.viewEmailBlast.bind(null, blast)}
                    style={{
                        color: '#1E88E5'
                    }}
                />
            </div>
        )
    }

    renderEmailBlasts() {
        if (this.props.loading) {
            return [];
        }else {
            return this.props.emailBlast.map((blast) => {
                return {
                    email: blast.content,
                    recipients: blast.recipients,
                    recipientCount: blast.recipient_count,
                    dateSent: blast.date_sent.toDateString(),
                    actions: this.createButton(blast)
                }
            });
        }
    }

    createRadioButton(network, color){
        return (
            <Radio
                value={network}
                ripple
            >
                <div
                    style={{
                        paddingLeft: '20px',
                        paddingBottom: '5px',
                        color: 'white',
                        background: color,
                        fontSize: '15px',
                        width: '100px',
                    }}
                >
                    {network}
                </div>
            </Radio>
        )
    }

    handleInputChange(evt) {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields });
    }

    checkProps(){
        const fields = this.state.fields;
        return fields.network === '' || fields.content === '';
    }

    sendEmailBlast(){
        emailBlast.call({
            content: this.state.fields.content,
            recipients: this.state.fields.network
        }, (error, response) => {
            if (error) {
                // console.log(error);
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error, 'Error sending emails');
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Emails sent successfully.', 'Success');
            }
        });

        //clear the contents of the field
        this.setState({
            fields: {
                content: '',
                network: ''
            }
        });
    }

    render() {
        return (
            <Card shadow={0} style={{minWidth: '900px'}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '55px',
                        background: '#229388',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>Users List</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#424242',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    {this.showDialog()}
                    <Grid>
                        <Cell col={7}>
                            <Textfield
                                style={{
                                    minWidth: '300px'
                                }}
                                onChange={this.handleInputChange}
                                rows={3}
                                label="Enter mail to send here"
                                name="content"
                                value={this.state.fields.content}
                            />
                            <Button
                                raised colored ripple
                                style={{
                                    width: '150px',
                                    marginTop: '15px'
                                }}
                                disabled={this.checkProps()}
                                onClick={this.sendEmailBlast}
                            >
                                <Icon name="send" />
                            </Button>
                        </Cell>
                        <Cell col={5}>
                            <label>Select Recipients</label><br/>
                            <RadioGroup name="network" value={this.state.fields.network} onChange={this.handleInputChange}>
                                {this.createRadioButton("ALL", "#5E35B1")}<br/>
                                {this.createRadioButton("AIRTEL", "#C2185B")}<br/>
                                {this.createRadioButton("ETISALAT", "#229388")}<br/>
                                {this.createRadioButton("GLO", "#00E676")}<br/>
                                {this.createRadioButton("MTN", "#FFB300")}
                            </RadioGroup>
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={12}>
                            <DataTable
                                shadow={0}
                                selectable
                                rows={this.renderEmailBlasts()}
                                style={{
                                    minWidth: '800px'
                                }}
                            >
                                <TableHeader name="email" tooltip="Email" style={{width:'300px'}}>Email</TableHeader>
                                <TableHeader name="recipients" tooltip="Recipients" style={{width:'50px'}}>Recipients</TableHeader>
                                <TableHeader name="recipientCount" tooltip="Recipient Count" style={{width:'50px'}}>Recipient Count</TableHeader>
                                <TableHeader name="dateSent" tooltip="Date Sent" style={{width:'50px'}}>Date Sent</TableHeader>
                                <TableHeader name="actions" tooltip="actions">Actions</TableHeader>
                            </DataTable>
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/admin" />
                </CardMenu>
            </Card>
        );
    }
}

EmailBlast.propTypes = {
    emailBlast: PropTypes.array.isRequired,
    loading: PropTypes.bool
}

export default EmailBlast;