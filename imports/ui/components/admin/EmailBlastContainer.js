/**
 * Created by ikedi on 27/09/2016.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import EmailBlastForm from './EmailBlastForm';
import { EmailBlast } from '../../../api/users/collections';

export default EmailBlastContainer = createContainer(({ params }) => {
    const emailBlastHandle = Meteor.subscribe('email-blast.all');
    const loading = !emailBlastHandle.ready();
    const emailBlast = EmailBlast.find().fetch();
    return {
        emailBlast,
        loading
    };
}, EmailBlastForm);