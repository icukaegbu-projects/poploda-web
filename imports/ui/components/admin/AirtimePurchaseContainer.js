/**
 * Created by ikedi on 23/09/2016.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import AirtimePurchaseForm from './AirtimePurchaseForm';
import { Transactions } from '../../../api/users/collections';

export default AirtimePurchaseContainer = createContainer(({ params }) => {
    const airtimeHandle = Meteor.subscribe('transactions.all');
    const loading = !airtimeHandle.ready();
    const airtimePurchase = Transactions.find().fetch();
    return {
        loading,
        airtimePurchase
    };
}, AirtimePurchaseForm);