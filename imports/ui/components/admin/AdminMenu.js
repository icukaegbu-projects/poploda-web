/**
 * Created by ikedi on 21/09/2016.
 */
import React, { Component } from 'react';
import { Grid, Cell, Icon, Button } from 'react-mdl';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';

class AdminMenu extends Component {
    constructor(props){
        super(props);

        this.router = props.router;

        this.menu = this.menu.bind(this);
        this.createButton = this.createButton.bind(this);
    }

    menu(link) {
        return (link === 'dashboard') ? FlowRouter.go('/') : FlowRouter.go(`${link}`);
    }

    createButton(text, color, icon, route) {
        return (
            <div>
                <Button
                    style={{
                        background: color,
                        color: 'white',
                        minWidth: '175px',
                        width: '175px',
                        padding: '10px',
                        height: '50px'
                    }}
                    onClick={this.menu.bind(null,route)}
                >
                    <Icon
                        name={`${icon}`}
                        style={{
                            color: 'white',
                        }}
                    />
                    {text}
                </Button>

            </div>
        )
    }

    render() {
        return (
            <div>
                <Grid>
                    <Cell col={2} phone={12}>
                        {this.createButton('Dashboard', "#C2185B", "dashboard", "admin-dashboard")}
                    </Cell>
                    <Cell col={2} phone={12}>
                        {this.createButton("Users","#5E35B1","contacts", "admin-users")}
                    </Cell>
                    <Cell col={2} phone={12}>
                        {this.createButton('Airtime Purchase', "#FFB300", "settings_cell", "admin-airtime-purchase")}
                    </Cell>
                    <Cell col={2} phone={12}>
                        {this.createButton('Wallet Top-up', "#2962FF", "launch", "admin-wallet-topup")}
                    </Cell>
                    <Cell col={2} phone={12}>
                        {this.createButton('Email Blast', "#229388", "toc", "admin-email-blast")}
                    </Cell>
                </Grid>
            </div>
        )
    }
}

export default AdminMenu;

{/*<div>*/}
    {/*<FABButton*/}
        {/*style={{*/}
            {/*background: color,*/}
            {/*color: 'white'*/}
        {/*}}*/}
        {/*onClick={this.menu.bind(null,route)}*/}
    {/*>*/}
        {/*<Icon name={`${icon}`}/>*/}
    {/*</FABButton>*/}
    {/*<div style={{*/}
        {/*display: 'inline-block',*/}
        {/*color: 'white',*/}
        {/*background: color,*/}
        {/*padding: '10px',*/}
        {/*paddingLeft: '15px',*/}
        {/*marginLeft: '-10px',*/}
        {/*width: '70px',*/}
        {/*minWidth: '80px'*/}
    {/*}}>*/}
        {/*{text}*/}
    {/*</div>*/}
{/*</div>*/}