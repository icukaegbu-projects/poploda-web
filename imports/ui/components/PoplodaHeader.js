/**
 * Created by ikedi on 02/07/2016.
 */
import React, { Component } from 'react';
import { Header, Navigation, HeaderRow } from 'react-mdl';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
import { Roles } from 'meteor/alanning:roles';
//import { Bert } from 'meteor/themeteorchef:bert';

class PopLodaHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loggedIn: false
        }

        this.isLoggedIn = this.isLoggedIn.bind(this);
        this.logout = this.logout.bind(this);
        this.login = this.login.bind(this);
        this.admin = this.admin.bind(this);
        this.user = this.user.bind(this);
    }

    login() {
        return FlowRouter.go('/login')
    }

    admin(){
        return FlowRouter.go('admin-dashboard')
    }

    user() {
        return FlowRouter.go('user-home')
    }

    logout() {
        Meteor.logout((error) => {
            if (error) {
                toastr.options = {"positionClass":'toast-top-right'};
                toastr.error(error.reason, 'Logout Error');
            } else {
                toastr.options = {"positionClass":'toast-top-right'};
                toastr.info('Logout Successful', 'Success');

                this.setState({ loggedIn: false });

                FlowRouter.go('/');
            }
        })
    }

    isLoggedIn() {
        if (Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'admin')){
            return (
                <Navigation>
                    <a href='' onClick={this.admin} style={{fontWeight: '800'}}>Admin</a>
                    <a href='' onClick={this.user} style={{fontWeight: '800'}}>User</a>
                    <a href='' onClick={this.logout} style={{fontWeight: '800'}}>Logout</a>
                </Navigation>
            )
        }else if (Meteor.userId()){
            return (
                <Navigation>
                    <a href='' onClick={this.logout} style={{fontWeight: '800'}}>Logout</a>
                </Navigation>
            )
        }else{
            return (
                ''
            )
        }
    }

    render() {
        return (
            <Header transparent waterfall hideTop>
                <HeaderRow title={
                             <a href="/" style={{
                                textDecoration: 'none',
                                color: 'white',
                                 fontSize: '40px',
                                 fontWeight: '800'
                             }}>Poploda</a>
                        }
                         style={{color: 'white'}}
                >
                    {
                        Meteor.userId()
                        ? ''
                         : <Navigation>
                            <a href='' onClick={this.login} style={{fontWeight: '800'}}>Login</a>
                        </Navigation>
                    }
                </HeaderRow>
                {
                    Meteor.userId()
                    ? <HeaderRow>
                        {this.isLoggedIn()}
                    </HeaderRow> : ''
                }

            </Header>
        )
    }
}

export default PopLodaHeader;


// <Header
//     transparent
//     title={
//         <a href="/" style={{
//             textDecoration: 'none',
//             color: 'white',
//             fontSize: '30px',
//             fontWeight: '800'
//         }}>Poploda</a>
//     }
//     style={{color: 'white'}}
// >
//     {this.isLoggedIn()}
//     {/*<Navigation>*/}
//     {/*{this.isLoggedIn()}*/}
//     {/*</Navigation>*/}
// </Header>

// isLoggedIn() {
//     //console.log(Meteor.userId());
//     if (Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'admin')){
//         return (
//             <Grid>
//                 <Cell col={3} phone={4}>
//                     {this.renderAdmin()}
//                 </Cell>
//                 <Cell col={3} phone={4}>
//                     {this.renderUser()}
//                 </Cell>
//                 <Cell col={3} phone={4}>
//                     {this.renderLogout()}
//                 </Cell>
//                 <IconButton name="mood" colored raised/>
//             </Grid>
//         )
//     }else if (Meteor.userId()){
//         return (
//             <Grid>
//                 <Cell col={3} phone={12}>
//                     {this.renderLogout()}
//                 </Cell>
//                 <IconButton name="mood" colored raised/>
//             </Grid>
//         )
//     }else{
//         return (
//             <Grid>
//                 <Cell col={3} phone={12}>
//                     {this.renderLogin()}
//                 </Cell>
//             </Grid>
//         )
//     }
// }


// class PopLodaHeader extends Component {
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             loggedIn: false
//         }
//
//         this.isLoggedIn = this.isLoggedIn.bind(this);
//         this.logout = this.logout.bind(this);
//         this.login = this.login.bind(this);
//         this.admin = this.admin.bind(this);
//         this.user = this.user.bind(this);
//         this.renderLogin = this.renderLogin.bind(this);
//         this.renderLogout = this.renderLogout.bind(this);
//         this.renderAdmin = this.renderAdmin.bind(this);
//         this.renderUser = this.renderUser.bind(this);
//     }
//
//     login() {
//         return FlowRouter.go('/login')
//     }
//
//     admin(){
//         return FlowRouter.go('admin-dashboard')
//     }
//
//     user() {
//         return FlowRouter.go('user-home')
//     }
//
//     logout() {
//         Meteor.logout((error) => {
//             if (error) {
//                 //console.log("There was an error:" + error.reason);
//                 // Bert.alert({
//                 //     title: 'Error',
//                 //     message: error.reason,
//                 //     type: 'danger',
//                 //     style: 'growl-top-right'
//                 // })
//                 toastr.options = {"positionClass":'toast-top-right'};
//                 toastr.error(error.reason, 'Logout Error');
//             } else {
//                 // Bert.alert({
//                 //     title: 'Success',
//                 //     message: 'Logout Successful',
//                 //     type: 'info',
//                 //     style: 'growl-top-right'
//                 // });
//                 toastr.options = {"positionClass":'toast-top-right'};
//                 toastr.info('Logout Successful', 'Success');
//
//                 this.setState({ loggedIn: false });
//
//                 FlowRouter.go('/');
//             }
//         })
//     }
//
//     renderLogout() {
//         return (
//             <Button
//                 accent
//                 onClick={this.logout}
//                 style={{marginLeft: '15px', marginRight: '15px', background: '#FFFFFF'}}>
//                 Logout
//             </Button>
//         )
//     }
//
//     renderLogin() {
//         return (
//             <Button
//                 accent
//                 onClick={this.login}
//                 style={{marginLeft: '15px', marginRight: '15px', background: '#FFFFFF'}}>
//                 Login
//             </Button>
//         )
//     }
//
//     renderUser() {
//         return (
//             <Button
//                 accent
//                 onClick={this.user}
//                 style={{marginLeft: '15px', marginRight: '15px', background: '#FFFFFF'}}>
//                 User Dashboard
//             </Button>
//         )
//     }
//
//     renderAdmin() {
//         return (
//             <Button
//                 accent
//                 onClick={this.admin}
//                 style={{marginLeft: '15px', marginRight: '15px', background: '#FFFFFF'}}>
//                 Admin Dashboard
//             </Button>
//         )
//     }
//
//     isLoggedIn() {
//         //console.log(Meteor.userId());
//         if (Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'admin')){
//             return (
//                 <div>
//                     {this.renderAdmin()}
//                     {this.renderUser()}
//                     {this.renderLogout()}
//                     <IconButton name="mood" colored raised/>
//                 </div>
//             )
//         }else if (Meteor.userId()){
//             return (
//                 <div>
//                     {this.renderLogout()}
//                     <IconButton name="mood" colored raised/>
//                 </div>
//             )
//         }else{
//             return (
//                 <div>
//                     {this.renderLogin()}
//                 </div>
//             )
//         }
//     }
//
//     render() {
//         return (
//             <Header
//                 transparent
//                 title={
//                     <a href="/" style={{
//                         textDecoration: 'none',
//                         color: 'white',
//                         fontSize: '30px',
//                         fontWeight: '800'
//                     }}>Poploda</a>
//                 }
//                 style={{color: 'white'}}
//             >
//                 <Navigation>
//                     {this.isLoggedIn()}
//                 </Navigation>
//             </Header>
//             // <Navbar
//             //     brand={
//             //         <NavItem href="/">Poploda</NavItem>
//             //     }
//             //     className='transparent_nav' right>
//             //     {
//             //         this.isLoggedIn()
//             //     }
//             // </Navbar>
//         )
//     }
// }