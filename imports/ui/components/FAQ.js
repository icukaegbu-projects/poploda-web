import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardText, CardMenu,
     IconButton, Grid, Cell } from 'react-mdl';

import StatusCard from './cards/StatusCard';

class FAQ extends Component {
    constructor(props) {
        super(props);

        this.checkScreenWidth = this.checkScreenWidth.bind(this);
    }

    checkScreenWidth(){
        return screen.width >= 720 ? '900px' : '300px'
    }

    render(){
        return (
            <Card shadow={0} style={{minWidth: '300px', width: this.checkScreenWidth()}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '75px',
                        background: '#546E7A',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>FAQs</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#fff',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    <Grid>
                        <Cell col={4}>
                            <StatusCard
                                color="#1E88E5"
                                headerSize="17"
                                contentTextColor="#FFF"
                                title="What is Poploda?"
                                content="The Poploda platform provides easy airtime top-up using the web and our native mobile apps.
Using these 2 channels, you can send airtime directly to supported lines in Nigeria."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#FFB300"
                                headerSize="17"
                                title="How can I access Poploda?"
                                content="Visit our website - www.Poploda.com on web and smartphones. Download our Native Apps from Google Play store or iOS store."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#AA47BB"
                                headerSize="17"
                                title="How can I register on Poploda?"
                                content="Visit www.Poploda.com and click on “Register”. Enter all the required details. On successful registration, a confirmation email will be sent to your inbox. Note that Registration is free."
                            />
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={4}>
                            <StatusCard
                                color="#229388"
                                headerSize="17"
                                title="How can I make payment on Poploda"
                                content="Poploda currently supports all debit cards issued by the banks in Nigeria (MasterCard, Visa, and Verve). Ensure your card is activated for online transactions, as per instructions from your bank."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#546E7A"
                                headerSize="17"
                                title="Are my transactions secured?"
                                content="The Poploda platform is safe and secure; we do not divulge customer information to any other party."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#5E35B1"
                                headerSize="17"
                                title="Are there any recurring charges for this service?"
                                content="Poploda does not charge any extra fees for our core transactions: Buy airtime and Prepaid Card Funding are all at face value. No additional charges."
                            />
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={4}>
                            <StatusCard
                                color="#FFB300"
                                headerSize="17"
                                title="Is there a threshold (minimum and maximum amounts) per transaction?"
                                content="The minimum recharge amount is N50 while the maximum amount for recharge is N20,000."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#AA47BB"
                                headerSize="17"
                                title="I entered a wrong number when trying to buy airtime? What should I do?"
                                content="Kindly contact your GSM provider on a possible resolution."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#1E88E5"
                                headerSize="17"
                                title="I made a successful payment but have not received the service? What should I do?"
                                content="Contact support@poploda.com with your payment reference."
                            />
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={4}>
                            <StatusCard
                                color="#5E35B1"
                                headerSize="17"
                                title="I made an unsuccessful payment but I have been debited? What should I do?"
                                content="Your bank should reverse the transaction within 24 hours. If the funds are yet to be reversed, contact support@poploda.com or call ???"
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#229388"
                                headerSize="17"
                                title="How do I reset my password?"
                                content="Click on the “Reset password” link on the home page."
                            />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#546E7A"
                                headerSize="17"
                                title="Why can’t I log into my account?"
                                content="Please contact support@poploda.com or call ??? if you cannot access your account."
                            />
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/" />
                </CardMenu>
            </Card>
        )
    }
}

export default FAQ;