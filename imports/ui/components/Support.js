import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardActions, CardText, CardMenu,
    IconButton, Grid, Cell } from 'react-mdl';

import StatusCard from './cards/StatusCard';

class Support extends Component {
    constructor(props) {
        super(props);

        this.checkScreenWidth = this.checkScreenWidth.bind(this);
    }

    checkScreenWidth(){
        return screen.width >= 720 ? '900px' : '300px'
    }

    render(){
        return (
            <Card shadow={0} style={{minWidth: '300px', width: this.checkScreenWidth()}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '75px',
                        background: '#5E35B1',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>Help & Support</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#fff',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    <Grid>
                        <Cell col={8}>
                            <Card
                                shadow={0}
                                style={{
                                    width: '400px',
                                    height: '400px',
                                    background: 'url(images/8.jpg) no-repeat center / cover',
                                    margin: 'auto'
                                }}>
                                <CardTitle expand />
                                <CardActions
                                    style={{
                                        height: '52px',
                                        padding: '16px',
                                        background: 'rgba(0,0,0,0.2)'
                                    }}>
                                    <span style={{
                                        color: '#fff',
                                        fontSize: '14px',
                                        fontWeight: '500'
                                    }}>
                                        Image.jpg
                                    </span>
                                </CardActions>
                            </Card>
                        </Cell>
                        <Cell col={4}>
                            <StatusCard
                                color="#5E35B1"
                                title="How can we be of service?"
                                content="We are here to help you. Do call any of our service lines below for assistance."
                            />
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/" />
                </CardMenu>
            </Card>
        )
    }
}

export default Support;