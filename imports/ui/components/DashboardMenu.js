import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import MenuCard from './cards/MenuCard';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
//import { Bert } from 'meteor/themeteorchef:bert';

class DashboardMenu extends Component {
    constructor(props){
        super(props);

        this.router = props.router;

        this.menu = this.menu.bind(this);
    }

    menu(link) {
        //return FlowRouter.go(`${link}`);

        //temporary
        if(link === '/fund-wallet' ){
            // Bert.alert({
            //     title: 'Pending Feature',
            //     message: 'Coming soon',
            //     type: 'warning',
            //     style: 'growl-bottom-left'
            // })
            toastr.options = {"positionClass":'toast-bottom-left'};
            toastr.warning('Feature in progress', 'Pending Feature')
        }else{
            return FlowRouter.go(`${link}`);
        }

    }

    render() {
        return (
            <div>
                <Grid>
                    <Cell col={4}>
                        <MenuCard
                            color="#1E88E5"
                            title="Buy Airtime"
                            onClick={this.menu.bind(null,'/recharge')}
                            icon="credit_card"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#FFB300"
                            title="Signup"
                            onClick={this.menu.bind(null,'/signup')}
                            icon="store"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#AA47BB"
                            title="Login"
                            onClick={this.menu.bind(null,'/login')}
                            icon="toc"
                        />
                    </Cell>
                </Grid>
                <Grid>
                    <Cell col={4}>
                        <MenuCard
                            color="#229388"
                            title="Fund Wallet"
                            onClick={this.menu.bind(null,'/fund-wallet')}
                            icon="settings_cell"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#546E7A"
                            title="FAQs"
                            onClick={this.menu.bind(null,'/faqs')}
                            icon="work"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#5E35B1"
                            title="Help & Support"
                            onClick={this.menu.bind(null,'/support')}
                            icon="perm_phone_msg"
                        />
                    </Cell>
                </Grid>
            </div>
        )
    }
}

// DashboardMenu.propTypes = {
//     router: React.PropTypes.func
// }

export default DashboardMenu;

{/*<div>*/}
    {/*<Row>*/}
        {/*<Col s={12} m={4}>*/}
            {/*<MenuCard*/}
                {/*color="blue darken-1"*/}
                {/*title="Buy Airtime"*/}
                {/*onClick={this.menu('/buy-airtime')}*/}
                {/*icon="credit_card"*/}
            {/*/>*/}
        {/*</Col>*/}
        {/*<Col s={12} m={4}>*/}
            {/*<MenuCard*/}
                {/*color="amber darken-1"*/}
                {/*title="Signup"*/}
                {/*onClick={this.menu.bind(null,'/signup')}*/}
                {/*icon="store"*/}
            {/*/>*/}
        {/*</Col>*/}
        {/*<Col s={12} m={4}>*/}
            {/*<MenuCard*/}
                {/*color="purple lighten-1"*/}
                {/*title="Login"*/}
                {/*onClick={this.menu.bind(null,'/login')}*/}
                {/*icon="toc"*/}
            {/*/>*/}
        {/*</Col>*/}
    {/*</Row>*/}
    {/*<Row>*/}
        {/*<Col s={12} m={4}>*/}
            {/*<MenuCard*/}
                {/*color="teal lighten-1"*/}
                {/*title="Fund Wallet"*/}
                {/*onClick={this.menu.bind(null,'/fund-wallet')}*/}
                {/*icon="settings_cell"*/}
            {/*/>*/}
        {/*</Col>*/}
        {/*<Col s={12} m={4}>*/}
            {/*<MenuCard*/}
                {/*color="blue-grey darken-1"*/}
                {/*title="FAQs"*/}
                {/*onClick={this.menu.bind(null,'/faqs')}*/}
                {/*icon="work"*/}
            {/*/>*/}
        {/*</Col>*/}
        {/*<Col s={12} m={4}>*/}
            {/*<MenuCard*/}
                {/*color="deep-purple darken-1"*/}
                {/*title="Help & Support"*/}
                {/*onClick={this.menu.bind(null,'/support')}*/}
                {/*icon="perm_phone_msg"*/}
            {/*/>*/}
        {/*</Col>*/}
    {/*</Row>*/}
{/*</div>*/}