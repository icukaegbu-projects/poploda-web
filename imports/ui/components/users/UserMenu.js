import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import MenuCard from '../cards/MenuCard';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
// import { Bert } from 'meteor/themeteorchef:bert';

class UserMenu extends Component {
    constructor(props){
        super(props);

        this.router = props.router;

        this.menu = this.menu.bind(this);
    }

    menu(link) {
        //return FlowRouter.go(`${link}`);

        //temporary
        if(link === '/support' || link === '/fund-wallet' ||link === '/faqs'){
            // Bert.alert({
            //     title: 'Pending Feature',
            //     message: 'Coming soon',
            //     type: 'warning',
            //     style: 'growl-bottom-left'
            // })
            toastr.options = {"positionClass":'toast-bottom-left'};
            toastr.warning('Coming Soon', 'Pending Feature');
        }else{
            return FlowRouter.go(`${link}`);
        }

    }

    render() {
        return (
            <div>
                <Grid>
                    <Cell col={4}>
                        <MenuCard
                            color="#5E35B1"
                            title="Recharge"
                            onClick={this.menu.bind(null,'user-recharge')}
                            icon="settings_cell"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#FFB300"
                            title="History"
                            onClick={this.menu.bind(null,'user-history')}
                            icon="toc"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#229388"
                            title="Profile"
                            onClick={this.menu.bind(null,'user-profile')}
                            icon="contacts"
                        />
                    </Cell>
                </Grid>
                <Grid>
                    <Cell col={4}>
                        <MenuCard
                            color="#1E88E5"
                            title="Home"
                            onClick={this.menu.bind(null,'user-home')}
                            icon="store"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#546E7A"
                            title="Wallet"
                            onClick={this.menu.bind(null,'user-wallet')}
                            icon="work"
                        />
                    </Cell>
                    <Cell col={4}>
                        <MenuCard
                            color="#AA47BB"
                            title="Change Password"
                            onClick={this.menu.bind(null,'change-password')}
                            icon="edit"
                        />
                    </Cell>
                </Grid>
            </div>
        )
    }
}

export default UserMenu;