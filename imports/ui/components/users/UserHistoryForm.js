/**
 * Created by ikedi on 14/07/2016.
 */
/**
 * Created by ikedi on 13/07/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Grid, Cell, DataTable, TableHeader, IconButton,
        Card, CardText, CardTitle, CardMenu } from 'react-mdl';

import StatusCard from '../cards/StatusCard';

class UserHistoryForm extends Component {
    constructor(props) {
        super(props);

        this.renderTransactions = this.renderTransactions.bind(this);
        this.calculateWeekly = this.calculateWeekly.bind(this);
        this.calculateMonthly = this.calculateMonthly.bind(this);
        this.calculateAnnually = this.calculateAnnually.bind(this);
        this.checkScreenWidth = this.checkScreenWidth.bind(this);
    }

    checkScreenWidth(){
        return screen.width >= 720 ? '550px' : '300px';
    }

    renderTransactions(){
        // console.log(this.props.loading)
        // if(!this.props.loading){
        //     console.log(this.props.transactions)
        // }
        if (this.props.loading) {
            return [];
        }else {
            return this.props.transactions.map((ap) => {
                return {
                    phone: ap.phone,
                    network: ap.network,
                    txnRef: ap.txn_ref,
                    amount: ap.amount / 100,
                    status: ap.txn_status,
                    transactionDate: ap.txn_time
                }
            });
        }
    }

    calculateAnnually() {
        return '#23,000.00'
    }

    calculateMonthly() {
        //select the current month and get data that matches the month and calculate their sum
        return '#3,000.00';
    }

    calculateWeekly() {
        //get current week range and select data that matches the week and calculate their sum
        return '#700.00'
    }

    render() {
        return (
            <Card shadow={0} style={{minWidth: '300px', width: this.checkScreenWidth()}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '75px',
                        background: '#FFB300',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>History</h4>
                </CardTitle>
                <CardText style={{
                    paddingRight: '0px',
                    color: '#424242',
                    fontWeight: '400',
                    fontSize: '15px'
                }}>
                    <Grid>
                        <Cell col={4}>
                            <StatusCard color='#26C6DA' title='This Week' content={this.calculateWeekly()} size='small' />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard color='#FB8C00' title='Last Month' content={this.calculateMonthly()} size='small' />
                        </Cell>
                        <Cell col={4}>
                            <StatusCard color='#424242' title='Last Quarter' content={this.calculateAnnually()} size='small' />
                        </Cell>
                    </Grid>
                    <Grid>
                        <Cell col={12}>
                            <DataTable
                                shadow={0}
                                rows={this.renderTransactions()}
                                style={{
                                    minWidth: '300px'
                                }}
                            >
                                <TableHeader name="phone" tooltip="Phone Number" style={{width:'50px'}}>Phone</TableHeader>
                                <TableHeader name="network" tooltip="Network" style={{width:'50px'}}>Network</TableHeader>
                                <TableHeader name="amount" tooltip="Amount" style={{width:'50px'}}>Amount</TableHeader>
                                <TableHeader name="status" tooltip="Status" style={{width:'100px'}}>T/Status</TableHeader>
                                <TableHeader name="transactionDate" tooltip="Transaction Date" style={{width:'80px'}}>T/Date</TableHeader>
                            </DataTable>
                        </Cell>
                    </Grid>
                </CardText>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/user" />
                </CardMenu>
            </Card>
        );
    }
}

UserHistoryForm.propTypes = {
    loading: PropTypes.bool,
    transactions: PropTypes.array.isRequired
}

export default UserHistoryForm;

// class History extends Component {
//     constructor(props){
//         super(props);
//
//         this.state = {
//             phone: '',
//             amount: '',
//             email: ''
//         }
//
//         this.createCard = this.createCard.bind(this);
//         this.createCardGrid = this.createCardGrid.bind(this);
//         this.createTableRow = this.createTableRow.bind(this);
//         this.createSearchBar = this.createSearchBar.bind(this);
//     }
//
//     createCard(month, amount, color) {
//         return (
//             <Col s={4}>
//                 <CardPanel
//                     className={`stats-card ${color} white-text`}
//                     style={{height: '75px', width: '75px', padding: '10px'}}>
//                     <span style={{fontWeight: '900', paddingBottom: '20px', fontSize: '25px'}}>{month}</span><br/>
//                     <span>{amount}</span>
//                 </CardPanel>
//             </Col>
//         )
//     }
//
//     createCardGrid(){
//         //loop through the data and create the grid of cards
//         const colors = ['teal lighten-2', 'blue-grey', 'purple lighten-3', 'orange darken-1',
//                         'cyan lighten-3', 'lime darken-1', 'red darken-1', 'brown darken-1',
//                         ' indigo', 'green accent-2', 'cyan lighten-1', 'grey darken-3'];
//
//         return (
//             <div>
//                 <Row>
//                     { this.createCard('Jan', 2000, colors[0])}
//                     { this.createCard('Feb', 1350, colors[1])}
//                     { this.createCard('Mar', 200, colors[2])}
//                 </Row>
//                 <Row>
//                     { this.createCard('Apr', 2000, colors[3])}
//                     { this.createCard('May', 1350, colors[4])}
//                     { this.createCard('Jun', 200, colors[5])}
//                 </Row>
//                 <Row>
//                     { this.createCard('Jul', 2000, colors[6])}
//                     { this.createCard('Aug', 1350, colors[7])}
//                     { this.createCard('Sep', 200, colors[8])}
//                 </Row>
//                 <Row>
//                     { this.createCard('Oct', 2000, colors[9])}
//                     { this.createCard('Nov', 1350, colors[10])}
//                     { this.createCard('Dec', 200, colors[11])}
//                 </Row>
//             </div>
//         )
//     }
//
//     createTableRow(){
//         //take an object and iterate over it
//         return (
//             <tr>
//                 <td>MTN</td>
//                 <td>08022932378</td>
//                 <td>1,200</td>
//                 <td>12/12/2016</td>
//             </tr>
//         )
//     }
//
//     createSearchBar() {
//         return (
//             <div>
//                 <Col s={12}>
//                     <Input s={9} type="text" label="Enter Date Range">
//                         <Icon>search</Icon>
//                     </Input>
//                     <Button>Search</Button>
//                 </Col>
//
//             </div>
//
//         )
//     }
//
//     render(){
//         return (
//             <Card
//                 className='white darken-1'
//                 textClassName='blue-text'
//                 title='History'
//             >
//                 <Row>
//                     { this.createSearchBar() }
//                 </Row>
//
//                 <Row>
//                     <Col s={7}>
//                         <h5>Total Purchases (By Month)</h5>
//                         <hr/>
//                         { this.createCardGrid() }
//                     </Col>
//                     <Col s={5}>
//                         <h5>Most Recent Purchases</h5>
//                         <hr/>
//                         <Table
//                             bordered={true} stripped={true}
//                             responsive={true} hoverable={true}>
//                             <thead>
//                             <tr>
//                                 <th data-field="network">Network</th>
//                                 <th data-field="phone">Phone</th>
//                                 <th data-field="amount">Amount</th>
//                                 <th data-field="date">Date</th>
//                             </tr>
//                             </thead>
//
//                             <tbody>
//                                 {this.createTableRow()}
//                                 {this.createTableRow()}
//                                 {this.createTableRow()}
//                                 {this.createTableRow()}
//                                 {this.createTableRow()}
//                                 {this.createTableRow()}
//                             </tbody>
//                         </Table>
//                     </Col>
//                 </Row>
//             </Card>
//
//         )
//     }
// }

// export default History;