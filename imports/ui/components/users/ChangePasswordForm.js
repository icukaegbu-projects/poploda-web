/**
 * Created by ikedi on 02/09/2016.
 */

import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardText,
    CardMenu, CardActions,
    IconButton, Button, Grid, Cell,
    Textfield } from 'react-mdl';

const checkProps = (props) => (
    props.newPassword === '' || props.confirmPassword === '' || props.oldPassword === ''
)

const checkScreenWidth = () => (
    screen.width >= 720 ? '475px' : '300px'
)

const ChangePasswordForm = (props) => {
    return (
        <form onSubmit={props.onSubmit}>
            <Card shadow={0} style={{minWidth: '300px', width: checkScreenWidth()}}>
                <CardTitle
                    style={{
                        color: '#fff',
                        height: '75px',
                        background: '#A746B8',
                        fontWeight: '600',
                        fontSize: '25px'
                    }}
                >
                    <h4>Change Password</h4>
                </CardTitle>
                <CardText style={{paddingRight: '0px'}}>
                    <Grid>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Current Password"
                                type="password"
                                name="oldPassword"
                                value={props.oldPassword}
                                onChange={props.handleInputChange}
                            >
                            </Textfield>
                        </Cell>

                        <hr/>

                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="New Password"
                                type="password"
                                name="newPassword"
                                value={props.newPassword}
                                onChange={props.handleInputChange}
                            >
                            </Textfield>
                        </Cell>
                        <Cell col={12} offsetDesktop={2}>
                            <Textfield
                                floatingLabel
                                label="Confirm Password"
                                type="password"
                                name="confirmPassword"
                                value={props.confirmPassword}
                                onChange={props.handleInputChange}
                            >
                            </Textfield>
                        </Cell>
                    </Grid>
                </CardText>
                <CardActions border>
                    <Grid style={{margin: '0px', padding: '0px'}}>
                        <Cell col={3} offsetDesktop={2}>
                            <Button
                                raised colored ripple
                                onClick={props.onSubmit}
                                disabled={checkProps(props)}
                            >
                                Change Password
                            </Button>
                        </Cell>
                        <Cell col={3} offsetDesktop={2}>
                            <Button
                                raised accent ripple
                                onClick={props.resetState}
                                type="reset">
                                Cancel
                            </Button>
                        </Cell>
                    </Grid>
                </CardActions>
                <CardMenu style={{color: '#fff'}}>
                    <IconButton name="home" href="/user" />
                </CardMenu>
            </Card>
        </form>
    )
}

ChangePasswordForm.propTypes = {
    oldPassword: PropTypes.string.isRequired,
    newPassword: PropTypes.string,
    confirmPassword: PropTypes.string,
    handleInputChange: PropTypes.func.isRequired,
    resetState: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
}

export default ChangePasswordForm;