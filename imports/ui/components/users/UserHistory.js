/**
 * Created by ikedi on 14/10/2016.
 */

import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import UserHistoryForm from './UserHistoryForm';
import { Transactions } from '../../../api/users/collections';

export default UserHistory = createContainer(({ params }) => {
    //const transactionsHandle = Meteor.subscribe('transactions.user', Meteor.userId());
    let user = Meteor.users.findOne(Meteor.userId());
    let queryParams = '';
    if(user.username === 'admin') {
        queryParams = "07030859941";
    }else{
        queryParams = user.username;
    }
    const transactionsHandle = Meteor.subscribe('transactions.user', queryParams);
    //const transactionsHandle = Meteor.subscribe('transactions.user', user.bio.phone);
    const loading = !transactionsHandle.ready();
    const transactions = Transactions.find().fetch();

    return {
        loading,
        transactions
    };
}, UserHistoryForm);