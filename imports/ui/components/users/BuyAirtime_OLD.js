/**
 * Created by ikedi on 26/08/2016.
 */
/**
 * Created by ikedi on 13/07/2016.
 */
import React, { Component, PropTypes } from 'react';
import BuyAirtimeForm from './BuyAirtimeForm';
import { queryAirVendAPI, queryInterSwitch, hashSHA512 } from '../../../api/users/methods';

import regex_validations from '../../../api/regex_validations';

class BuyAirtime_OLD extends Component {
    constructor(props){
        super(props);

        this.state = {
            phone: '',
            email: '',
            network: '',
            amount_naira: '',
            amount: '',
            product_id: '',
            currency: 0,
            site_redirect_url: '',
            txn_ref: '',
            hash: '',
            pay_item_id: '',
            mack_key: '',
            txn_time: '',
            errorMessage: ''
        }

        //this.createCard = this.createCard.bind(this);
        this.computeHash = this.computeHash.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleAmount = this.handleAmount.bind(this);
        this.handlePhone = this.handlePhone.bind(this);
        this.handleNetwork = this.handleNetwork.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.reset = this.reset.bind(this);
        this.generateTransactionRef = this.generateTransactionRef.bind(this);
        this.makePaymentWithInterSwitch = this.makePaymentWithInterSwitch.bind(this);
        this.buyCreditFromAirVend = this.buyCreditFromAirVend.bind(this);
    }

    componentDidMount() {
        //set product_id, mac_key, site_redirect_url
        //DEMO
        this.setState({
            site_redirect_url: 'http://www.poploda.com/complete',
            mac_key: 'D3D1D05AFE42AD50818167EAC73C109168A0F108F32645C8B59E897FA930DA44F9230910DAC9E20641823799A107A02068F7BC0F4CC41D2952E249552255710F',
            product_id: '6205',
            pay_item_id: '101',
            currency: 566
        })
    }

    computeHash() {
        //combine the 7 state items and use a SHA512 computation on it
        let { txn_ref, product_id, pay_item_id, amount, site_redirect_url, mac_key } = this.state;
        //let mac_key = this.props.mac_key;

        let hashString = txn_ref + product_id + pay_item_id + amount +
            site_redirect_url + mac_key;

        let hash = hashSHA512(hashString);

        this.setState({hash: hash});
        console.log('from computeHash: '+this.state.hash)

        return hash;
    }

    generateTransactionRef() {
        //generate ref from network + date (yyyy+mm+dd) + random number
        let pd = new Date();
        let txn_time = `${pd.getFullYear()}:${pd.getMonth()+1}:${pd.getDate()}
                        -${pd.getHours()}:${pd.getMinutes()}`;
        let random = Math.floor(100000 + Math.random() * 900000);
        let tref = this.state.network.toUpperCase() + pd.getFullYear() +
            (pd.getMonth()+1) + pd.getDate() + random;

        this.setState({
            txn_ref: tref,
            txn_time: txn_time
        });
        console.log('from generate: '+this.state.txn_ref);

        return tref;
    }

    submitForm(e) {
        e.preventDefault();

        if(this.validateForm()){
            //compute hash, generate transaction ref
            //save transaction to firebase
            // and make async call to Interswitch to make payment
            //if interswitch unsuccessful, report error and
            // update transaction in firebase with failed status
            //if interswitch successful, buy credit from AirVend
            //if AirVend successful, update transaction in firebase with Airvend returned details
            //and notify the user
            //if Airvend not successful, update transaction with failed status

            // if(this.makePaymentWithInterSwitch()){
            //     this.buyCreditFromAirVend()
            // }

            console.log('Testing Interswitch API');
            this.makePaymentWithInterSwitch()

            console.log('Testing AirVend API');
            this.buyCreditFromAirVend();

            //route to purchase complete page
            FlowRouter.go('/complete');
        }
    }

    makePaymentWithInterSwitch() {
        //compute hash, generate transaction ref
        let txn = this.generateTransactionRef();
        let hash = this.computeHash();

        let queryParams = {
            product_id: this.state.product_id,
            amount: this.state.amount,
            currency: this.state.currency,
            site_redirect_url: this.state.site_redirect_url,
            txn_ref: txn,
            hash: hash,
            pay_item_id: this.state.pay_item_id,
        }
        // let queryParams = {
        //     product_id: this.state.product_id,
        //     amount: this.state.amount,
        //     currency: this.state.currency,
        //     site_redirect_url: this.state.site_redirect_url,
        //     txn_ref: this.state.txn_ref,
        //     hash: this.state.hash,
        //     pay_item_id: this.state.pay_item_id,
        // }

        //force the component to update
        this.forceUpdate();

        //deal with returned data
        //asyncHelpers.queryInterSwitch(queryParams);
        queryInterSwitch.call(queryParams,  (message, result) => {
            if (message) {
                console.log('Interswitch API error: ', message);
                this.setState({
                    errorMessage: message.toString()
                });

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Interswitch API error');
            } else {
                console.log('Interswitch complete, calling Airvend to buy credit!');
                console.log(result);

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success(`Payment Successful. ${result}`, 'Success');

                //if user is logged in redirect to user page else to home
                // if(Meteor.userId()){
                //     FlowRouter.go('user')
                // }else{
                //     FlowRouter.go('/');
                // }
            }
        });

        //if transaction successful, return true
        return true;
    }

    buyCreditFromAirVend() {
        let networkid = 0;
        switch (this.state.network) {
            case 'AIRTEL':
                networkid = 1;
                break;
            case 'MTN':
                networkid = 2;
                break;
            case 'GLO':
                networkid = 3;
                break;
            case 'ETISALAT':
                networkid = 4;
                break;
            case 'VISAPHONE':
                networkid = 5;
                break;
            default:
                networkid = 6;
        }

        let queryParams = {
            msisdn: this.state.phone,
            networkid: 6,
            amount: this.state.amount_naira,
            type: 1,
            username: 'admin@poploda.com',
            password: 'Poploda5822'
        }

        //asyncHelpers.queryAirVend(queryParams);
        queryAirVendAPI.call(queryParams,  (message, result) => {
            if (message) {
                console.log('AirVend API error: ', message);
                this.setState({
                    errorMessage: message.toString()
                });

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'AirVend API error');
            } else {
                console.log('Purchase from Airvend complete, redirect!');

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success(`Purchase Successful. ${result}`, 'Success');

                //if user is logged in redirect to user page else to home
                // if(Meteor.userId()){
                //     FlowRouter.go('user')
                // }else{
                //     FlowRouter.go('/');
                // }
            }
        });
    }

    validateForm(){
        let ck_email = regex_validations.ck_email;
        let ck_phone = regex_validations.ck_phone; //validate that phone is 11 digits

        let { email, phone, amount_naira } = this.state;
        let errorMessage = '';

        if ( phone === '' || amount_naira === '' || email === '') {
            errorMessage += 'Email, Phone and/or Amount cannot be null.\n';
            this.setState({
                phone: '',
                amount_naira: '',
                email: '',
                errorMessage: errorMessage
            });
            console.log(errorMessage)
            return false;
        }

        //if amount is less than 50 or greater than 50,000, throw error
        if (amount_naira < 50 && amount_naira > 50000){
            errorMessage += 'Amount cannot be less than N50.00 or more than N50,000.00.\n';
            this.setState({
                amount_naira: '',
                errorMessage: errorMessage
            });
            console.log(errorMessage)
            //return false;
        }

        //if above validations passed,
        // clear errorMessage before checking regular expressions
        //this.clearErrorAndSignup();

        //validate regular expressions
        if (!ck_email.test(email)) {
            errorMessage += "Please enter a valid Email Address.\n";
            this.setState({
                email: ''
            });
        }

        if (!ck_phone.test(phone)) {
            errorMessage += "Please enter a valid Phone Number.\n";
            this.setState({
                phone: ''
            });
        }
        console.log(errorMessage)

        //if errorMessage is not null, clear the state and return
        if(errorMessage !== '' ){
            //set this.state.errorMessage to errorMessage
            this.setState({errorMessage: errorMessage});
            return false;
        }

        //all validations passed, clear errorMessage
        this.setState({errorMessage: ''});
        console.log('All validations passed')
        return true;
    }

    handleNetwork(e){
        //this.clearErrorAndSignup();
        this.setState({network: e.currentTarget.value});
    }

    handleEmail(e){
        //this.clearErrorAndSignup();
        this.setState({email: e.target.value})
    }

    handleAmount(e){
        //this.clearErrorAndSignup();
        //convert to a number and assign
        let amount_naira = Number.parseInt(e.target.value);
        let amount = amount_naira * 100;
        //console.log(amount_naira)
        this.setState({
            amount_naira: amount_naira,
            amount: amount
        })
    }

    handlePhone(e){
        //this.clearErrorAndSignup();
        this.setState({phone: e.target.value})
    }

    reset(e) {
        e.preventDefault();
        this.setState({
            phone: '',
            email: '',
            network: '',
            amount_naira: '',
            amount: '',
            errorMessage: ''
        });
    }

    render(){
        return (
            <form name="BuyAirtime_OLD" action="https://tlsdebug.interswitchng.com" method="post">
                <BuyAirtimeForm
                    handleEmail={this.handleEmail}
                    handleAmount={this.handleAmount}
                    handlePhone={this.handlePhone}
                    handleNetwork={this.handleNetwork}
                    reset={this.reset}
                    email={this.state.email}
                    phone={this.state.phone}
                    amount={this.state.amount_naira}
                    errorMessage={this.state.errorMessage}/>

                <input type="hidden" name="product_id" value={this.state.product_id}/>
                <input type="hidden" name="amount" value={this.state.amount}/>
                <input type="hidden" name="currency" value={this.state.currency}/>
                <input type="hidden" name="site_redirect_url" value={this.state.site_redirect_url}/>
                <input type="hidden" name="txn_ref" value={this.state.txn_ref}/>
                <input type="hidden" name="hash" value={this.state.hash}/>
                <input type="hidden" name="pay_item_id" value={this.state.pay_item_id}/>
            </form>
        )
    }
}

// BuyAirtime.propTypes = {
//     mac_key: PropTypes.string.isRequired,
//     product_id: PropTypes.string.isRequired,
//     pay_item_id: PropTypes.string.isRequired
// }

export default BuyAirtime_OLD;

// generateRefAndHash() {
//     let txn = this.generateTransactionRef();
//     let hash = this.computeHash();
//
//     //store transaction in DB
//     let queryParams = {
//         product_id: this.state.fields.product_id,
//         amount: this.state.fields.amount,
//         currency: this.state.fields.currency,
//         txn_time: this.state.fields.txn_time,
//         txn_ref: txn,
//         hash: hash,
//         pay_item_id: this.state.fields.pay_item_id,
//     }
//
//     //call method to store the data
// }
//
// computeHash() {
//     //combine the 7 state items and use a SHA512 computation on it
//     const fields = this.state.fields;
//     let { txn_ref, product_id, pay_item_id, amount, site_redirect_url, mac_key } = fields;
//     //let mac_key = this.props.mac_key;
//
//     let hashString = txn_ref + product_id + pay_item_id + amount +
//         site_redirect_url + mac_key;
//
//     let hash = hashSHA512(hashString);
//
//     fields.hash = hash;
//     this.setState({ fields });
//
//     return hash;
// }
//
// generateTransactionRef() {
//     //generate ref from network + date (yyyy+mm+dd) + random number
//     const fields = this.state.fields;
//     let pd = new Date();
//     let txn_time = `${pd.getFullYear()}:${pd.getMonth()+1}:${pd.getDate()}
//                         -${pd.getHours()}:${pd.getMinutes()}`;
//     let random = Math.floor(100000 + Math.random() * 900000);
//     let tref = fields.network.toUpperCase() + pd.getFullYear() +
//         (pd.getMonth()+1) + pd.getDate() + random;
//
//     fields.txn_ref = tref;
//     fields.txn_time = txn_time;
//
//     this.setState({ fields });
//
//     return tref;
// }
//
// submitForm(e) {
//     //e.preventDefault();
//
//     if(this.validateForm()){
//         //compute hash, generate transaction ref
//         //save transaction to firebase
//         // and make async call to Interswitch to make payment
//         //if interswitch unsuccessful, report error and
//         // update transaction in firebase with failed status
//         //if interswitch successful, buy credit from AirVend
//         //if AirVend successful, update transaction in firebase with Airvend returned details
//         //and notify the user
//         //if Airvend not successful, update transaction with failed status
//
//         // if(this.makePaymentWithInterSwitch()){
//         //     this.buyCreditFromAirVend()
//         // }
//
//         console.log('Testing Interswitch API');
//         this.makePaymentWithInterSwitch()
//
//         console.log('Testing AirVend API');
//         this.buyCreditFromAirVend();
//
//         //route to purchase complete page
//         FlowRouter.go('/complete');
//     }
// }
//
// makePaymentWithInterSwitch() {
//     //compute hash, generate transaction ref
//     const fields = this.state.fields;
//     let txn = this.generateTransactionRef();
//     let hash = this.computeHash();
//
//     let queryParams = {
//         product_id: fields.product_id,
//         amount: fields.amount,
//         currency: fields.currency,
//         site_redirect_url: fields.site_redirect_url,
//         txn_ref: txn,
//         hash: hash,
//         pay_item_id: fields.pay_item_id,
//     }
//
//     //force the component to update
//     this.forceUpdate();
//
//     //deal with returned data
//     //asyncHelpers.queryInterSwitch(queryParams);
//     queryInterSwitch.call(queryParams,  (message, result) => {
//         if (message) {
//             console.log('Interswitch API error: ', message);
//             fields.errorMessage = message.toString();
//             this.setState({ fields });
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.error(message.reason, 'Interswitch API error');
//         } else {
//             console.log('Interswitch complete, calling Airvend to buy credit!');
//             console.log(result);
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.success(`Payment Successful. ${result}`, 'Success');
//
//             //if user is logged in redirect to user page else to home
//             // if(Meteor.userId()){
//             //     FlowRouter.go('user')
//             // }else{
//             //     FlowRouter.go('/');
//             // }
//         }
//     });
//
//     //if transaction successful, return true
//     return true;
// }
//
// buyCreditFromAirVend() {
//     const fields = this.state.fields;
//     let networkid = 0;
//     switch (fields.network) {
//         case 'AIRTEL':
//             networkid = 1;
//             break;
//         case 'MTN':
//             networkid = 2;
//             break;
//         case 'GLO':
//             networkid = 3;
//             break;
//         case 'ETISALAT':
//             networkid = 4;
//             break;
//         case 'VISAPHONE':
//             networkid = 5;
//             break;
//         default:
//             networkid = 6;
//     }
//
//     let queryParams = {
//         msisdn: fields.phone,
//         networkid: 6,
//         amount: fields.amount_naira,
//         type: 1,
//         username: 'admin@poploda.com',
//         password: 'Poploda5822'
//     }
//
//     //asyncHelpers.queryAirVend(queryParams);
//     queryAirVendAPI.call(queryParams,  (message, result) => {
//         if (message) {
//             console.log('AirVend API error: ', message);
//             fields.errorMessage = message.toString();
//             this.setState({ fields });
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.error(message.reason, 'AirVend API error');
//         } else {
//             console.log('Purchase from Airvend complete, redirect!');
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.success(`Purchase Successful. ${result}`, 'Success');
//
//             //if user is logged in redirect to user page else to home
//             // if(Meteor.userId()){
//             //     FlowRouter.go('user')
//             // }else{
//             //     FlowRouter.go('/');
//             // }
//         }
//     });
// }
//
// validateForm(){
//     const fields = this.state.fields;
//     let ck_email = regex_validations.ck_email;
//     let ck_phone = regex_validations.ck_phone; //validate that phone is 11 digits
//
//     let { email, phone, amount_naira } = fields;
//     let errorMessage = '';
//
//     if ( phone === '' || amount_naira === '' || email === '') {
//         fields.errorMessage += 'Email, Phone and/or Amount cannot be null.\n';
//         fields.phone = '';
//         fields.amount_naira = '';
//         fields.email = '';
//
//         this.setState({ fields });
//         console.log(errorMessage)
//         return false;
//     }
//
//     //if amount is less than 50 or greater than 50,000, throw error
//     if (amount_naira < 50 && amount_naira > 50000){
//         fields.errorMessage += 'Amount cannot be less than N50.00 or more than N50,000.00.\n';
//         fields.amount_naira = '';
//         this.setState({ fields });
//         console.log(errorMessage)
//         //return false;
//     }
//
//     //if above validations passed,
//     // clear errorMessage before checking regular expressions
//     //this.clearErrorAndSignup();
//
//     //validate regular expressions
//     if (!ck_email.test(email)) {
//         fields.errorMessage += "Please enter a valid Email Address.\n";
//         this.setState({ fields });
//     }
//
//     if (!ck_phone.test(phone)) {
//         fields.errorMessage += "Please enter a valid Phone Number.\n";
//         this.setState({ fields });
//     }
//     console.log(errorMessage)
//
//     //if errorMessage is not null, clear the state and return
//     if(errorMessage !== '' ){
//         //set this.state.errorMessage to errorMessage
//         //this.setState({errorMessage: errorMessage});
//         return false;
//     }
//
//     //all validations passed, clear errorMessage
//     fields.errorMessage = '';
//     this.setState({ fields });
//     console.log('All validations passed')
//     return true;
// }
