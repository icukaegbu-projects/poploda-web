/**
 * Created by ikedi on 26/07/2016.
 */

import React, { PropTypes } from 'react';
import { Card, CardText, CardTitle, CardMenu, IconButton,
    Cell, Grid, Button, Textfield, Radio, RadioGroup, Switch } from 'react-mdl';

const card = (title, color) => {
    return (
        <div
            style={{
                height: '45px',
                width: '100px',
                padding: '5px',
                paddingLeft: '15px',
                background: color,
                fontSize: '22px',
                color: '#FFF',
                fontWeight: '600'
            }}>
            {title}
        </div>
    )
}

const createCard = (network, color) => {
    return (
        <Radio
            ripple
            value={network}
        >
            {card(network, color)}
        </Radio>
    )
}

const checkProps = (props) => (
    props.amount === '' || props.phone === '' || props.network === ''
)

const checkScreenWidth = () => (
    screen.width >= 720 ? '475px' : '300px'
)

const BuyAirtimeForm = (props) => {
    return (
        <Card shadow={0} style={{minWidth: '300px', width: checkScreenWidth()}}>
            <CardTitle
                style={{
                    color: '#fff',
                    height: '60px',
                    background: '#5E35B0',
                    fontWeight: '600',
                    fontSize: '20px'
                }}
            >
                <h4>Buy Airtime</h4>
            </CardTitle>
            <CardText style={{paddingRight: '0px'}}>
                <Grid>
                    <Cell col={4} phone={12}>
                        <Grid>
                            <Cell col={12}>
                                <RadioGroup name="network" value={props.network} onChange={props.handleInputChange}>
                                    { createCard('MTN', '#FFC107', )}
                                    { createCard('Airtel', '#E53935')}
                                    { createCard('Glo', '#81C784')}
                                    { createCard('Etisalat', '#558B2F')}
                                </RadioGroup>
                            </Cell>
                            <Cell col={12}>
                                {card('Wallet Balance:', '#FF5722')}
                            </Cell>
                        </Grid>
                        
                    </Cell>
                    <Cell col={7} offsetDesktop={1} phone={12}>
                        <Grid>
                            <Cell col={12}>
                                <Textfield
                                    floatingLabel
                                    pattern="^\d{11}$"
                                    error="Phone must be 11 digits"
                                    label="Phone"
                                    type="phone"
                                    value={props.phone}
                                    onChange={props.handleInputChange}
                                    name="phone"
                                >
                                </Textfield>
                            </Cell>
                            <Cell col={12}>
                                <Textfield
                                    floatingLabel
                                    pattern="^\d{11}$"
                                    error="Amount not less than 100 and not more than 10000"
                                    label="Amount"
                                    type="number"
                                    value={props.amount_naira}
                                    onChange={props.handleInputChange}
                                    name="amount_naira"
                                >
                                </Textfield>
                            </Cell>
                            <Cell col={12}>
                                <Textfield
                                    floatingLabel
                                    pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"
                                    error="Email must contain @ symbol and a domain name"
                                    label="Email"
                                    type="email"
                                    value={props.email}
                                    onChange={props.handleInputChange}
                                    name="email"
                                >
                                </Textfield>
                            </Cell>
                            <Cell col={12}>
                                <Switch id="walletSwitch">Pay with Wallet</Switch>
                            </Cell>
                            <Grid style={{margin: '0px', padding: '0px'}}>
                                <Cell col={2}>
                                    <Button
                                        raised colored ripple
                                        onClick={props.submitForm}
                                        disabled={checkProps(props)}
                                        type="submit"
                                    >
                                        Pay
                                    </Button>
                                </Cell>
                                <Cell col={2} offsetDesktop={4}>
                                    <Button
                                        raised accent ripple
                                        onClick={props.reset}
                                        type="reset"
                                    >
                                        Clear
                                    </Button>
                                </Cell>
                            </Grid>
                        </Grid>
                    </Cell>
                </Grid>
                <Grid>
                    <Cell col={3}>
                        <p
                            style={{
                                width: '120px',
                                height: '75px',
                                background: 'url(../images/interswitch3.jpg) no-repeat center / contain',
                                margin: 'auto'
                            }}></p>
                    </Cell>
                    <Cell col={3}>
                        <p
                            style={{
                                width: '75px',
                                height: '75px',
                                background: 'url(../images/mastercard.png) no-repeat center / contain',
                                margin: 'auto'
                            }}></p>
                    </Cell>
                    <Cell col={3}>
                        <div
                            style={{
                                width: '75px',
                                height: '75px',
                                background: 'url(../images/visa.png) no-repeat center / contain',
                                margin: 'auto'
                            }}></div>
                    </Cell>
                    <Cell col={3}>
                        <div
                            style={{
                                width: '75px',
                                height: '75px',
                                background: 'url(../images/verve.jpg) no-repeat center / contain',
                                margin: 'auto'
                            }}></div>
                    </Cell>
                </Grid>
            </CardText>
            <CardMenu style={{color: '#fff'}}>
                { Meteor.userId()
                    ? <IconButton name="home" href="/user" />
                    : <IconButton name="home" href="/" />
                }
            </CardMenu>
        </Card>
    )
}

BuyAirtimeForm.propTypes = {
    handleInputChange: PropTypes.func.isRequired,
    submitForm: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,

    email: PropTypes.string.isRequired,
    amount_naira: PropTypes.number.isRequired,
    phone: PropTypes.string.isRequired,
    network: PropTypes.string.isRequired,
    errorMessage: PropTypes.string,
    loading: PropTypes.bool.isRequired
}

export default BuyAirtimeForm;

// disabled={checkProps(props)}