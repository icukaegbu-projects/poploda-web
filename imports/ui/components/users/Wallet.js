 /**
 * Created by ikedi on 14/07/2016.
 */

 import React, { Component } from 'react';
 import { Card, Col, Row, Button,
     Input, Icon, CardPanel, Table } from 'react-materialize';
 import StatusCard from '../cards/StatusCard';

 class Wallet extends Component {
     constructor(props){
         super(props);

         this.state = {
             card: '',
             expiry: '',
             pin: '',
             cvv: '',
             addCard: false
         }

         this.saveCard = this.saveCard.bind(this);
         this.createCard = this.createCard.bind(this);
         this.createAddCardForm = this.createAddCardForm.bind(this);
         this.createWalletHistoryTable = this.createWalletHistoryTable.bind(this);
         this.createTableRow = this.createTableRow.bind(this);
         this.createRegisteredCards = this.createRegisteredCards.bind(this);
         this.clearState = this.clearState.bind(this);
     }

     saveCard(card){
         //save the card to firebase. if successful, display message and hide form
         this.setState({addCard: false});
     }

     clearState(){
         this.setState({
             card: '',
             expiry: '',
             pin: '',
             cvv: '',
             addCard: false
         })
     }

     createCard(month, amount, color) {
         return (
             <Col s={4}>
                 <CardPanel
                     className={`stats-card ${color} white-text`}
                     style={{height: '75px', width: '75px', padding: '10px'}}>
                     <span style={{fontWeight: '900', paddingBottom: '20px', fontSize: '25px'}}>{month}</span><br/>
                     <span>{amount}</span>
                 </CardPanel>
             </Col>
         )
     }

     createAddCardForm(){
         //take an object and iterate over it
         return (
             <Card>
                 <h5>Add Card</h5>
                 <hr/>
                 <Input
                     s={11}
                     label="Card Number"
                     type="text"
                     value={this.state.card}
                     onChange={(e) => {this.setState({card: e.value})}}>
                     <Icon>user</Icon>
                 </Input>
                 <Input
                     s={11}
                     label="Expiry"
                     type="text"
                     value={this.state.expiry}
                     onChange={(e) => {this.setState({expiry: e.value})}}>
                     <Icon>user</Icon>
                 </Input>
                 <Input
                     s={11}
                     label="PIN"
                     type="text"
                     value={this.state.pin}
                     onChange={(e) => {this.setState({pin: e.value})}}>
                     <Icon>user</Icon>
                 </Input>
                 <Input
                     s={11}
                     label="CVV"
                     type="text"
                     value={this.state.cvv}
                     onChange={(e) => {this.setState({cvv: e.value})}}>
                     <Icon>user</Icon>
                 </Input>
                 <Row style={{margin: '0px', padding: '0px'}}>
                     <Col s={2} offset="s1">
                         <Button onClick={this.saveCard}>
                             Add
                         </Button>
                     </Col>
                     <Col s={2} offset="s2">
                         <Button onClick={this.clearState}>
                             Clear
                         </Button>
                     </Col>
                 </Row>
             </Card>
         )
     }

     createTableRow(){
         //take an object and iterate over it
         return (
             <tr>
                 <td>****-****-****-2378</td>
                 <td>1,200</td>
                 <td>12/12/2016</td>
             </tr>
         )
     }

     createWalletHistoryTable(){
         return (
             <Card>
                 <h6>Wallet History</h6>
                 <Table
                     bordered={true} stripped={true}
                     responsive={true} hoverable={true}>
                     <thead>
                     <tr>
                         <th data-field="card">Card #</th>
                         <th data-field="amount">Amount</th>
                         <th data-field="date">Date</th>
                     </tr>
                     </thead>

                     <tbody>
                     {this.createTableRow()}
                     {this.createTableRow()}
                     {this.createTableRow()}
                     {this.createTableRow()}
                     {this.createTableRow()}
                     {this.createTableRow()}
                     </tbody>
                 </Table>
             </Card>
         )
     }

     createRegisteredCards() {
         return (
             <Card>
                 <h6>Registered Cards</h6>
                 <hr/>
                 <StatusCard
                     cardType="weather-card"
                     cardSize="small"
                     cardColor="amber lighten-2"
                     textColor="amber-text text-lighten-5"/>
                 <StatusCard
                     cardType="stats-card"
                     cardSize="small"
                     cardColor="blue-grey"
                     textColor="red-text text-lighten-2"/>
             </Card>
         )
     }

     render(){
         return (
             <Card
                 className='white darken-1'
                 textClassName='blue-text'
                 title='Wallet'
             >
                 { this.state.addCard ? '' :
                     <Button onClick={() => this.setState({addCard: true})}>Add Card</Button>
                 }
                 <Row>
                     <Col s={6}>
                         { this.state.addCard ?
                             this.createAddCardForm() :
                             <div>
                                 <StatusCard/>
                                 { this.createWalletHistoryTable() }
                             </div>
                         }
                     </Col>
                     <Col s={6}>
                         { this.createRegisteredCards() }
                     </Col>
                 </Row>
             </Card>

         )
     }
 }

 export default Wallet;