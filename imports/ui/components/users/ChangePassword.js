import React, { Component, PropTypes } from 'react';
import ChangePasswordForm from './ChangePasswordForm';

class ChangePassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                oldPassword: '',
                newPassword: '',
                confirmPassword: ''
            }
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitPasswordChange = this.submitPasswordChange.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    handleInputChange(evt) {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields });
    }

    resetState(){
        const fields = this.state.fields;

        fields.oldPassword = '';
        fields.newPassword = '';
        fields.confirmPassword = '';
        this.setState({ fields });
    }

    submitPasswordChange(evt){
        evt.preventDefault();
        const fields = this.state.fields;

        //confirm oldPassword and newPassword are not the same
        if (fields.oldPassword === fields.newPassword){
            toastr.options = {"positionClass":'toast-top-left'};
            toastr.error('Old and New Password cannot be the same', 'Similar Passwords.');

            //clear the fields and update state
            this.resetState()

            return;
        }

        //confirm newPassword and confirmPassword are the same
        if (fields.newPassword !== fields.confirmPassword){
            toastr.options = {"positionClass":'toast-top-left'};
            toastr.error('New Password does not match', 'Unmatched New Passwords.');

            //clear the fields and update state
            this.resetState()

            return;
        }

        //make call to change password and display corresponding error messages
        Accounts.changePassword(fields.oldPassword, fields.newPassword, (error) => {
            if(error){
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'Password Error');

                //clear the fields and update state
                this.resetState()
            }else{
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Password Change Successful.', 'Success');

                //clear the fields and update state
                this.resetState()

                FlowRouter.go('user-home');
            }
        })
    }

    render(){
        return (
            <ChangePasswordForm
                oldPassword={this.state.fields.oldPassword}
                newPassword={this.state.fields.newPassword}
                confirmPassword={this.state.fields.confirmPassword}
                handleInputChange={this.handleInputChange}
                resetState={this.resetState}
                onSubmit={this.submitPasswordChange}
                />
        )
    }
}

export default ChangePassword;