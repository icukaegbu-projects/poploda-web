/**
 * Created by ikedi on 21/08/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardText,
    CardMenu, CardActions,
    IconButton, Button, Grid, Cell } from 'react-mdl';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
import StatusCard from '../cards/StatusCard';
import { updateTransactionAndCallAirVend } from '../../../api/users/methods';

class PurchaseComplete extends Component {
    constructor(props) {
        super(props);
        //console.log(document.referrer)

        this.state = {
            fields: {
                referrer: document.referrer
            }
        }

        this.processReferrer = this.processReferrer.bind(this);
        this.updateTransaction = this.updateTransaction.bind(this);
    }

    componentDidMount() {
        this.processReferrer();
    }

    processReferrer() {
        // let referrer = document.referrer;
        let domain = this.state.fields.referrer.split("?");
        let parts = domain[1].split("&");
        let transactionInfo = {};
        parts.forEach((obj) => {
            let objPart = obj.split("=");
            if(objPart[0] !== 'url') {
                transactionInfo[objPart[0]] = objPart[1];
            }
        })
        console.log('Logging domain')
        console.log(transactionInfo)

        this.updateTransaction(transactionInfo)
    }

    updateTransaction(queryParams) {
        console.log('Logging queryParams')
        console.log(JSON.stringify(queryParams));

        updateTransactionAndCallAirVend.call(queryParams,  (error, result) => {
            if (error) {
                console.log('Save Error: ', error);

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error, 'Transaction Save error');
            } else {
                console.log('Transaction save complete!');
                console.log(result)

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Transaction Saved Successful.', 'Success');
            }
        });

        //if transaction successful, return true
        return true;
    }

    render(){
        return (
            <form>
                <Card shadow={0} style={{width: '512px'}}>
                    <CardTitle
                        style={{
                            color: '#fff',
                            height: '75px',
                            background: '#C6FF00',
                            fontWeight: '600',
                            fontSize: '25px'
                        }}
                    >
                        <h4>Purchase Complete</h4>
                    </CardTitle>
                    <CardText style={{paddingRight: '0px'}}>
                        <Grid>
                            <Cell col={6}>
                                <StatusCard
                                    color="#229388"
                                    headerSize="17"
                                    title="Payment Status"
                                    content={`Transaction Ref: 12333ER.\nNetwork: MTN.\nAmount: #100`}
                                />
                            </Cell>
                            <Cell col={6}>
                                <StatusCard
                                    color="#546E7A"
                                    headerSize="17"
                                    title="Airtime Status"
                                    content="....Loading airtime"
                                />
                            </Cell>
                        </Grid>
                    </CardText>
                    <CardActions border>
                        <Grid style={{margin: '0px', padding: '0px'}}>
                            <Cell col={6} offset={2}>
                                <Button
                                    raised colored ripple
                                >
                                    Transaction Complete
                                </Button>
                            </Cell>
                        </Grid>
                    </CardActions>
                    <CardMenu style={{color: '#fff'}}>
                        { Meteor.userId()
                            ? <IconButton name="home" href="/" />
                            : <IconButton name="home" href="/user" />
                        }
                    </CardMenu>
                </Card>
            </form>
        )
    }
}

export default PurchaseComplete;


// submitForm(e) {
//     //e.preventDefault();
//
//     if(this.validateForm()){
//         //compute hash, generate transaction ref
//         //save transaction to firebase
//         // and make async call to Interswitch to make payment
//         //if interswitch unsuccessful, report error and
//         // update transaction in firebase with failed status
//         //if interswitch successful, buy credit from AirVend
//         //if AirVend successful, update transaction in firebase with Airvend returned details
//         //and notify the user
//         //if Airvend not successful, update transaction with failed status
//
//         // if(this.makePaymentWithInterSwitch()){
//         //     this.buyCreditFromAirVend()
//         // }
//
//         console.log('Testing Interswitch API');
//         this.makePaymentWithInterSwitch()
//
//         console.log('Testing AirVend API');
//         this.buyCreditFromAirVend();
//
//         //route to purchase complete page
//         FlowRouter.go('/complete');
//     }
// }
//
// makePaymentWithInterSwitch() {
//     //compute hash, generate transaction ref
//     const fields = this.state.fields;
//     let txn = this.generateTransactionRef();
//     let hash = this.computeHash();
//
//     let queryParams = {
//         product_id: fields.product_id,
//         amount: fields.amount,
//         currency: fields.currency,
//         site_redirect_url: fields.site_redirect_url,
//         txn_ref: txn,
//         hash: hash,
//         pay_item_id: fields.pay_item_id,
//     }
//
//     //force the component to update
//     this.forceUpdate();
//
//     //deal with returned data
//     //asyncHelpers.queryInterSwitch(queryParams);
//     queryInterSwitch.call(queryParams,  (message, result) => {
//         if (message) {
//             console.log('Interswitch API error: ', message);
//             fields.errorMessage = message.toString();
//             this.setState({ fields });
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.error(message.reason, 'Interswitch API error');
//         } else {
//             console.log('Interswitch complete, calling Airvend to buy credit!');
//             console.log(result);
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.success(`Payment Successful. ${result}`, 'Success');
//
//             //if user is logged in redirect to user page else to home
//             // if(Meteor.userId()){
//             //     FlowRouter.go('user')
//             // }else{
//             //     FlowRouter.go('/');
//             // }
//         }
//     });
//
//     //if transaction successful, return true
//     return true;
// }
//
// buyCreditFromAirVend() {
//     const fields = this.state.fields;
//     let networkid = 0;
//     switch (fields.network) {
//         case 'AIRTEL':
//             networkid = 1;
//             break;
//         case 'MTN':
//             networkid = 2;
//             break;
//         case 'GLO':
//             networkid = 3;
//             break;
//         case 'ETISALAT':
//             networkid = 4;
//             break;
//         case 'VISAPHONE':
//             networkid = 5;
//             break;
//         default:
//             networkid = 6;
//     }
//
//     let queryParams = {
//         msisdn: fields.phone,
//         networkid: 6,
//         amount: fields.amount_naira,
//         type: 1,
//         username: 'admin@poploda.com',
//         password: 'Poploda5822'
//     }
//
//     //asyncHelpers.queryAirVend(queryParams);
//     queryAirVendAPI.call(queryParams,  (message, result) => {
//         if (message) {
//             console.log('AirVend API error: ', message);
//             fields.errorMessage = message.toString();
//             this.setState({ fields });
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.error(message.reason, 'AirVend API error');
//         } else {
//             console.log('Purchase from Airvend complete, redirect!');
//
//             toastr.options = {"positionClass":'toast-top-left'};
//             toastr.success(`Purchase Successful. ${result}`, 'Success');
//
//             //if user is logged in redirect to user page else to home
//             // if(Meteor.userId()){
//             //     FlowRouter.go('user')
//             // }else{
//             //     FlowRouter.go('/');
//             // }
//         }
//     });
// }
//
// validateForm(){
//     const fields = this.state.fields;
//     let ck_email = regex_validations.ck_email;
//     let ck_phone = regex_validations.ck_phone; //validate that phone is 11 digits
//
//     let { email, phone, amount_naira } = fields;
//     let errorMessage = '';
//
//     if ( phone === '' || amount_naira === '' || email === '') {
//         fields.errorMessage += 'Email, Phone and/or Amount cannot be null.\n';
//         fields.phone = '';
//         fields.amount_naira = '';
//         fields.email = '';
//
//         this.setState({ fields });
//         console.log(errorMessage)
//         return false;
//     }
//
//     //if amount is less than 50 or greater than 50,000, throw error
//     if (amount_naira < 50 && amount_naira > 50000){
//         fields.errorMessage += 'Amount cannot be less than N50.00 or more than N50,000.00.\n';
//         fields.amount_naira = '';
//         this.setState({ fields });
//         console.log(errorMessage)
//         //return false;
//     }
//
//     //if above validations passed,
//     // clear errorMessage before checking regular expressions
//     //this.clearErrorAndSignup();
//
//     //validate regular expressions
//     if (!ck_email.test(email)) {
//         fields.errorMessage += "Please enter a valid Email Address.\n";
//         this.setState({ fields });
//     }
//
//     if (!ck_phone.test(phone)) {
//         fields.errorMessage += "Please enter a valid Phone Number.\n";
//         this.setState({ fields });
//     }
//     console.log(errorMessage)
//
//     //if errorMessage is not null, clear the state and return
//     if(errorMessage !== '' ){
//         //set this.state.errorMessage to errorMessage
//         //this.setState({errorMessage: errorMessage});
//         return false;
//     }
//
//     //all validations passed, clear errorMessage
//     fields.errorMessage = '';
//     this.setState({ fields });
//     console.log('All validations passed')
//     return true;
// }