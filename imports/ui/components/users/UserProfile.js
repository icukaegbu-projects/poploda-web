/**
 * Created by ikedi on 10/10/2016.
 */

import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { userUpdate } from '../../../api/users/methods';
import UserProfileForm from './UserProfileForm';

export default UserProfile = createContainer(({ params }) => {
    const usersHandle = Meteor.subscribe('users.one', Meteor.userId());
    //const usersHandle = Meteor.subscribe('users.all');
    const loading = !usersHandle.ready();
    const user = Meteor.users.findOne({
        _id: Meteor.userId()
    }, {
        fields: {
            "roles": 0,
            "createdAt": 0
        }
    });

    return {
        loading,
        user,
        userUpdate
    };
}, UserProfileForm);