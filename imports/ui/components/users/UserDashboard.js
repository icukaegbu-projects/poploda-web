/**
 * Created by ikedi on 12/07/2016.
 */
import React, { Component } from 'react';
import { Link } from 'react-router';
import { Card, Button,
    Col, Row, Icon } from 'react-materialize';
import UserHome from './UserHomeForm';
import UserProfile from './UserProfileForm';
import BuyAirtime from './BuyAirtime';
import History from './UserHistoryForm';
import Wallet from './Wallet';
import MenuCard from '../cards/MenuCard';

class UserDashboard extends Component {
    constructor(){
        super();

        this.state = {
            showWallet: false,
            showHistory: false,
            showProfilePage: false,
            showBuyAirtime: false,
            showUserHome: false
        }

        this.toggle = this.toggle.bind(this);
        //this.toggleSignup = this.toggleSignup.bind(this);
        this.showMenu = this.showMenu.bind(this);
        this.showHistory = this.showHistory.bind(this);
        this.showWallet = this.showWallet.bind(this);
        this.showProfilePage = this.showProfilePage.bind(this);
        this.showBuyAirtime = this.showBuyAirtime.bind(this);
        this.showUserHome = this.showUserHome.bind(this);
        this.renderView = this.renderView.bind(this);

    }

    componentDidMount() {
        this.setState({showUserHome: true});
    }

    toggle(display){
        switch(display){
            case 'WALLET':
                this.setState({
                    showWallet: true,
                    showHistory: false,
                    showProfilePage: false,
                    showBuyAirtime: false,
                    showUserHome: false
                });
                break;
            case 'HISTORY':
                this.setState({
                    showWallet: false,
                    showHistory: true,
                    showProfilePage: false,
                    showBuyAirtime: false,
                    showUserHome: false
                });
                break;
            case 'PROFILE':
                this.setState({
                    showWallet: false,
                    showHistory: false,
                    showProfilePage: true,
                    showBuyAirtime: false,
                    showUserHome: false
                });
                break;
            case 'RECHARGE':
                this.setState({
                    showWallet: false,
                    showHistory: false,
                    showProfilePage: false,
                    showBuyAirtime: true,
                    showUserHome: false
                });
                break;
            case '':
            default:
                this.setState({
                    showWallet: false,
                    showHistory: false,
                    showProfilePage: false,
                    showBuyAirtime: false,
                    showUserHome: true
                });
        }
    }

    showHistory() {
        //console.log('Login');
        return (
            <History toggle={this.toggle} />
        )
    }

    showProfilePage() {
        //console.log('Profile');
        return (
            <UserProfile toggle={this.toggle} />
        )
    }

    showWallet(){
        return (
            <Wallet/>
        )
    }

    showBuyAirtime(){
        return (
            <BuyAirtime/>
        )
    }

    showUserHome(){
        //console.log('user home called')
        return (
            <UserHome/>
        )
    }

    showMenu(){
        return (
            <div>
                <Row>
                    <Col s={12} m={6}>
                        <MenuCard
                            color="blue darken-1"
                            title="Home"
                            onClick={this.toggle}
                            icon="store"
                        />
                    </Col>
                    <Col s={12} m={6}>
                        <MenuCard
                            color="amber darken-1"
                            title="History"
                            onClick={this.toggle.bind(null, 'HISTORY')}
                            icon="toc"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col s={12} m={6}>
                        <MenuCard
                            color="purple lighten-1"
                            title="Wallet"
                            onClick={this.toggle.bind(null, 'WALLET')}
                            icon="work"
                        />
                    </Col>
                    <Col s={12} m={6}>
                        <MenuCard
                            color="teal lighten-1"
                            title="Recharge"
                            onClick={this.toggle.bind(null, 'RECHARGE')}
                            icon="settings_cell"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col s={12} m={6}>
                        <MenuCard
                            color="blue-grey darken-1"
                            title="Profile"
                            onClick={this.toggle.bind(null, 'PROFILE')}
                            icon="contacts"
                        />
                    </Col>
                </Row>
            </div>
        )
    }

    renderView() {
        if (this.state.showHistory) {
            return this.showHistory()
        }else if (this.state.showProfilePage) {
            return this.showProfilePage()
        }else if (this.state.showWallet) {
            return this.showWallet()
        }else if (this.state.showBuyAirtime) {
            return this.showBuyAirtime()
        }else {
            return this.showUserHome()
        }
    }

    render() {
        return (
            <Row>
                <Col s={12} m={4} l={4}>
                    {
                        this.showMenu()
                    }
                </Col>
                <Col s={12} m={8} l={8}>
                    {
                        this.renderView()
                    }
                </Col>
            </Row>
        )
    }
}

export default UserDashboard;