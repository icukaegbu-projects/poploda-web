/**
 * Created by ikedi on 13/07/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardActions, CardText, Icon, Button } from 'react-mdl';
//import StatusCard from '../cards/StatusCard';

class UserHome extends Component {
    constructor(props){
        super(props);

        this.displayInfo = this.displayInfo.bind(this);
        this.checkScreenWidth = this.checkScreenWidth.bind(this);
    }

    checkScreenWidth(){
        return screen.width >= 720 ? '450px' : '300px';
    }

    displayInfo(){
        if(!this.props.loading) {
            return this.props.user.bio.name
        }
    }

    render(){
        return (
            <Card shadow={0} style={{width: this.checkScreenWidth(), height: '450px', background: '#3E4EB8'}}>
                <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
                    <h4 style={{marginTop: '0'}}>
                        Welcome: {this.displayInfo()}<br />
                        Transactions Summary
                    </h4>
                </CardTitle>
                <CardText>

                </CardText>
                <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
                    <Button colored style={{color: '#fff'}}>More Details</Button>
                    <div className="mdl-layout-spacer"></div>
                    <Icon name="event" />
                </CardActions>
            </Card>
        )
    }
}

UserHome.propTypes = {
    loading: PropTypes.bool,
    user: PropTypes.object.isRequired
}

export default UserHome;

// class UserHome extends Component {
//     constructor(props){
//         super(props);
//
//         this.renderCollectionItem = this.renderCollectionItem.bind(this);
//     }
//
//     renderCollectionItem(date, amount) {
//         return (
//             <div>
//                 <span className="red-text text-lighten-2">{date}</span>
//                 <span style={{fontWeight: '900'}} className="blue-grey-text darken-4 right">{amount}</span>
//             </div>
//         )
//     }
//
//     render(){
//         return (
//             <Card>
//                 <Row>
//                     <Col s={6}>
//                         <StatusCard
//                             cardType="weather-card"
//                             cardSize="small"
//                             cardColor="amber lighten-2"
//                             textColor="amber-text text-lighten-5"/>
//                         <StatusCard
//                             cardType="stats-card"
//                             cardSize="small"
//                             cardColor="blue-grey"
//                             textColor="red-text text-lighten-2"/>
//                     </Col>
//                     <Col s={3}>
//                         <StatusCard />
//                         <StatusCard />
//                     </Col>
//                     <Col s={3}>
//                         <Collection>
//                             <h5 style={{paddingLeft: '15px'}}>Wallet History</h5>
//                             <CollectionItem>
//                                 {this.renderCollectionItem('20/12/16', 2000)}
//                             </CollectionItem>
//                             <CollectionItem>
//                                 {this.renderCollectionItem('20/12/16', 2000)}
//                             </CollectionItem>
//                             <CollectionItem>
//                                 {this.renderCollectionItem('20/12/16', 2000)}
//                             </CollectionItem>
//                             <CollectionItem>
//                                 {this.renderCollectionItem('20/12/16', 2000)}
//                             </CollectionItem>
//                             <CollectionItem>
//                                 {this.renderCollectionItem('20/12/16', 2000)}
//                             </CollectionItem>
//                         </Collection>
//                     </Col>
//                 </Row>
//             </Card>
//         )
//     }
// }