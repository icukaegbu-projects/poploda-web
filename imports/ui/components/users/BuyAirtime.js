/**
 * Created by ikedi on 13/07/2016.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import React, { Component, PropTypes } from 'react';
import BuyAirtimeForm from './BuyAirtimeForm';
import { STATUS, queryAirVendAPI, queryInterSwitch, hashSHA512 } from '../../../api/users/queries';
import { createTransaction } from '../../../api/users/methods';

//import regex_validations from '../../../api/regex_validations';

class BuyAirtimeContainer extends Component {
    constructor(props){
        super(props);

        this.state = {
            fields: {
                phone: '',
                email: '',
                network: '',
                amount_naira: 0,
                amount: 0,
                product_id: '',
                currency: 0,
                site_redirect_url: '',
                txn_ref: '',
                hash: '',
                pay_item_id: '',
                mac_key: '',
                txn_time: '',
                errorMessage: '',
                canSubmit: false,
                //data: {}
                loading: false
            }
        }

        this.computeHash = this.computeHash.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.reset = this.reset.bind(this);
        this.generateRefAndHash = this.generateRefAndHash.bind(this);
        this.generateTransactionRef = this.generateTransactionRef.bind(this);
        this.createTransaction = this.createTransaction.bind(this);
        // this.submitForm = this.submitForm.bind(this);
        // this.validateForm = this.validateForm.bind(this);
        //this.makePaymentWithInterSwitch = this.makePaymentWithInterSwitch.bind(this);
        // this.buyCreditFromAirVend = this.buyCreditFromAirVend.bind(this);
    }

    componentDidMount() {
        //set product_id, mac_key, site_redirect_url
        //DEMO
        const fields = this.state.fields;
        fields.site_redirect_url = 'https://www.poploda.com/complete';
        fields.mac_key = Meteor.settings.public.MAC_KEY,
        fields.product_id = '6205',
        fields.pay_item_id = '101',
        fields.currency = 566;

        this.setState({ fields });
    }

    componentWillReceiveProps(nextProps) {
        const fields = this.state.fields;

        // fields.data = {
        //     user: nextProps.user,
        //     loading: nextProps.loading
        // }
        fields.loading = nextProps.loading;

        if(nextProps.user){
            fields.phone= nextProps.user.bio.phone;
            fields.email = nextProps.user.emails[0].address;

            if(nextProps.user.bio.network === 'MTN'){
                fields.network = nextProps.user.bio.network;
            }else{
                //convert the first letter to capitals
                let str = nextProps.user.bio.network.toLowerCase();
                fields.network = str.charAt(0).toUpperCase() + str.slice(1)
            }

        }

        this.setState({ fields });
    }

    generateRefAndHash(e) {
        //e.preventDefault()
        const fields = this.state.fields;

        //compute transaction ref
        let txn = this.generateTransactionRef();
        fields.txn_ref = txn.tref;
        fields.txn_time = txn.txn_time;

        //compute hash
        let hash = this.computeHash(fields);
        fields.hash = hash;

        //store transaction in DB
        let queryParams = {
            product_id: fields.product_id,
            amount: fields.amount,
            currency: fields.currency,
            txn_time: fields.txn_time,
            txn_ref: fields.txn_ref,
            hash: fields.hash,
            pay_item_id: fields.pay_item_id,
            network: fields.network,
            phone: fields.phone,
            cust_id: fields.phone,
            cust_name: fields.email,
            txn_status: STATUS.Transaction_Initiated,
            isRegistered: Meteor.userId() ? true : false,
            userId: Meteor.userId() ? Meteor.userId() : 'NOT_REGISTERED',
        }

        //call method to store the data
        this.createTransaction(queryParams);

        this.setState( { fields });

        //clear the form
        //this.reset();
        //disable submit button
        // fields.canSubmit = false;
        // this.setState( { fields });
    }

    createTransaction(queryParams) {
        console.log('Logging queryParams')
        console.log(JSON.stringify(queryParams))
        //deal with returned data
        //asyncHelpers.queryInterSwitch(queryParams);
        createTransaction.call(queryParams,  (error) => {
            if (error) {
                console.log('Save Error: ', error);

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'Transaction Save error');
            } else {
                console.log('Transaction save complete!');

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Transaction Saved Successful.', 'Success');
            }
        });

        //if transaction successful, return true
        return true;
    }

    computeHash(fields) {
        //combine the 7 state items and use a SHA512 computation on it
        //const fields = this.state.fields;
        let { txn_ref, product_id, pay_item_id, amount, site_redirect_url, mac_key } = fields;

        let hashString = txn_ref + product_id + pay_item_id + amount + site_redirect_url + mac_key;

        let hash = hashSHA512(hashString);

        return hash;
    }

    generateTransactionRef() {
        //generate ref from network + date (yyyy+mm+dd) + random number
        const fields = this.state.fields;
        let pd = new Date();
        let txn_time = `${pd.getFullYear()}:${pd.getMonth()+1}:${pd.getDate()} - ${pd.getHours()}:${pd.getMinutes()}`;
        let random = Math.floor(100000 + Math.random() * 900000);
        let tref = fields.network.toUpperCase() + pd.getFullYear() + (pd.getMonth()+1) + pd.getDate() + random;

        return { tref, txn_time };
    }

    handleInputChange(evt) {
        const fields = this.state.fields;
        if(evt.target.name === 'amount_naira'){
            let amount_naira = Number.parseInt(evt.target.value);
            let amount = amount_naira * 100;
            fields.amount_naira = amount_naira;
            fields.amount = amount;
        }else{
            fields[evt.target.name] = evt.target.value
        }
        fields.canSubmit = fields.amount === '' || fields.phone === '' || fields.network === '';
        this.setState({ fields });
    }

    // disableSubmit() {
    //     const fields = this.state.fields;
    //     fields.canSubmit = props.amount === '' || props.phone === '' || props.network === '';
    //     this.setState({ fields })
    // }

    reset() {
        //e.preventDefault();
        const fields = this.state.fields;
        fields.phone = '';
        fields.email = '';
        fields.network = '';
        fields.amount_naira = 0;
        fields.amount = 0;
        fields.errorMessage = '';
        this.setState({ fields });
    }

    render(){
        return (
            <form name="BuyAirtime" action="https://stageserv.interswitchng.com/test_paydirect/pay" method="post">
                <BuyAirtimeForm
                    handleInputChange={this.handleInputChange}
                    submitForm={this.generateRefAndHash}
                    reset={this.reset}
                    email={this.state.fields.email}
                    phone={this.state.fields.phone}
                    amount_naira={this.state.fields.amount_naira}
                    network={this.state.fields.network}
                    errorMessage={this.state.fields.errorMessage}
                    loading={this.state.fields.loading}
                />

                <input type="hidden" name="product_id" value={this.state.fields.product_id}/>
                <input type="hidden" name="amount" value={this.state.fields.amount}/>
                <input type="hidden" name="currency" value={this.state.fields.currency}/>
                <input type="hidden" name="site_redirect_url" value={this.state.fields.site_redirect_url}/>
                <input type="hidden" name="txn_ref" value={this.state.fields.txn_ref}/>
                <input type="hidden" name="hash" value={this.state.fields.hash}/>
                <input type="hidden" name="pay_item_id" value={this.state.fields.pay_item_id}/>
                <input type="hidden" name="site_name" value="https://www.poploda.com"/>
                <input type="hidden" name="cust_id" value={this.state.fields.phone}/>
                <input type="hidden" name="cust_name" value={this.state.fields.email}/>
            </form>
        )
    }
}

//export default BuyAirtime;

export default BuyAirtime = createContainer(({ params }) => {
    //const usersHandle = Meteor.subscribe('users.one', Meteor.userId());
    const usersHandle = Meteor.subscribe('users.all');
    const loading = !usersHandle.ready();
    const user = Meteor.users.findOne({
        _id: Meteor.userId()
    }, {
        fields: {
            "roles": 0,
            "createdAt": 0
        }
    });

    return {
        loading,
        user
    };
}, BuyAirtimeContainer);
