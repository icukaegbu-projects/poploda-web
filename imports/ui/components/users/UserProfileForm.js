/**
 * Created by ikedi on 13/07/2016.
 */

import React, { Component, PropTypes } from 'react';
import { Card, CardTitle, CardText,
    CardMenu, CardActions,
    IconButton, Icon, Button, Grid, Cell,
    Textfield, Spinner, List, ListItem, ListItemContent } from 'react-mdl';


class UserProfileForm extends Component {
    constructor(props){
        super(props);

        this.state = {
            isViewing: true,
            user: null
        }

        this.displayEditing = this.displayEditing.bind(this);
        this.displayViewing = this.displayViewing.bind(this);
        this.display = this.display.bind(this);
        this.onEditClick = this.onEditClick.bind(this);
        this.onSaveClick = this.onSaveClick.bind(this);
        this.onCancelClick = this.onCancelClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.checkScreenWidth = this.checkScreenWidth.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            user: nextProps.user
        });
    }

    checkScreenWidth(){
        return screen.width >= 720 ? '512px' : '300px';
    }

    displayViewing(user) {
        return (
            <div>
                <ListItem>
                    <ListItemContent icon="person"><strong>Name:</strong> {user.bio.name}</ListItemContent>
                </ListItem>
                <ListItem>
                    <ListItemContent icon="phone"><strong>Phone:</strong> {user.bio.phone}</ListItemContent>
                </ListItem>
                <ListItem>
                    <ListItemContent icon="settings"><strong>Network:</strong> {user.bio.network}</ListItemContent>
                </ListItem>
            </div>
        )
    }

    displayEditing(user) {
        return (
            <div>
                <ListItem>
                    <ListItemContent icon="person">
                        <Textfield
                            floatingLabel
                            label="Name"
                            pattern="^[A-Za-z0-9 ]{3,20}$"
                            error="Name must be at least 3 letters"
                            type="text"
                            value={user.bio.name}
                            onChange={this.handleInputChange}
                            name="name"
                        >
                        </Textfield>
                    </ListItemContent>
                </ListItem>
                <ListItem>
                    <ListItemContent icon="phone">
                        <Textfield
                            floatingLabel
                            label="Phone"
                            pattern="^\d{11}$"
                            error="Phone must be 11 digits"
                            type="tel"
                            value={user.bio.phone}
                            onChange={this.handleInputChange}
                            name="phone"
                        >
                        </Textfield>
                    </ListItemContent>
                </ListItem>
                <ListItem>
                    <ListItemContent icon="settings">
                        <Textfield
                            floatingLabel
                            label="Network"
                            pattern="^[A-Za-z0-9 ]{3,20}$"
                            error="State not less than 6 and not more than 20"
                            type="text"
                            value={user.bio.network}
                            onChange={this.handleInputChange}
                            name="network"
                        >
                        </Textfield>
                    </ListItemContent>
                </ListItem>
            </div>
        )
    }

    display(){
        let user = this.state.user;
        return (
            <CardText style={{padding: '0px', margin: '0px'}}>
                <Grid>
                    <Cell col={11} offsetDesktop={1}>
                        <List>
                            <ListItem>
                                <ListItemContent icon="email"><strong>Email:</strong> {user.emails[0].address}</ListItemContent>
                            </ListItem>
                            <ListItem>
                                <ListItemContent icon="person"><strong>Username:</strong> {user.username}</ListItemContent>
                            </ListItem>
                            { this.state.isViewing ? this.displayViewing(user) : this.displayEditing(user) }
                        </List>
                    </Cell>
                </Grid>
            </CardText>
        )
    }

    onEditClick(e) {
        e.preventDefault();

        this.setState({ isViewing: false });
    }

    onCancelClick(e) {
        e.preventDefault();

        this.setState({
            isViewing: true
        });
    }

    onSaveClick(e) {
        e.preventDefault();

        //call method to save
        let bio = {
            name: this.state.user.bio.name,
            phone: this.state.user.bio.phone,
            network: this.state.user.bio.network
        }

        if (bio.name === '' || bio.phone === '' || bio.network === ''){
            toastr.options = {"positionClass":'toast-top-left'};
            toastr.error('Name, Phone or Network cannot be null', 'Invalid Data');

            return;
        }

        let params = {
            _id: this.state.user._id,
            bio: bio
        }

        this.props.userUpdate.call( params, (error, result) => {
            if (error) {
                //console.log(error);
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'User Update Error');

                //return user to previous value
                // this.setState({ user: this.props.user });
                // this.forceUpdate();
            } else {
                //console.log(result);
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('User Update Successful.', 'Success');

                //update state
                this.setState({ isViewing: true });
            }
        })


    }

    handleInputChange(evt) {
        const fields = this.state.user.bio;
        fields[evt.target.name] = evt.target.value;
        this.setState({ "user.bio": fields });
    }

    render() {
        return (
            <form>
                <Card shadow={0} style={{minWidth: '300px', width: this.checkScreenWidth(), padding: '0px'}}>
                    <CardTitle
                        style={{
                            color: '#fff',
                            height: '75px',
                            background: '#208A7F',
                            fontWeight: '600',
                            fontSize: '25px'
                        }}
                    >
                        <h4>Profile</h4>
                        { this.state.isViewing ? <p>(Click on Pencil icon to edit)</p> : '' }
                    </CardTitle>
                    { this.props.loading ? <div></div> : this.display() }
                    <CardMenu style={{color: '#fff'}}>
                        { this.state.isViewing
                            ? <IconButton name="edit" onClick={this.onEditClick} >Click to Edit</IconButton>
                            : <div>
                            <IconButton name="save" onClick={this.onSaveClick} />
                            <IconButton name="cancel" onClick={this.onCancelClick} />
                        </div>
                        }
                    </CardMenu>
                </Card>
            </form>
        )
    }
}

UserProfileForm.props = {
    loading: PropTypes.bool,
    user: PropTypes.object.isRequired,
    userUpdate: PropTypes.func.isRequired
}

export default UserProfileForm;