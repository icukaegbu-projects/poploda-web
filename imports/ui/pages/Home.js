// import '../styles/index';

import React, { Component } from 'react';
import Login from '../components/authentication/Login';
import Signup from '../components/authentication/Signup';
//import Carousel from '../components/Carousel';
import { Card, CardTitle, Button,
        Col, Row, Icon } from 'react-materialize';
import MenuCard from '../components/cards/MenuCard';
import BuyAirtime from '../components/users/BuyAirtime';

// import styles from '../styles';
// let transparentBg = styles.transparentBg;

class Home extends Component {
    constructor(){
        super();

        this.state = {
            showMenu: false,
            showBuyAirtime: false,
            showSignup: false,
            showLogin: false,
            showWallet: false,
            showFAQ: false,
            showHelp: false
        }

        this.toggle = this.toggle.bind(this);
        //this.toggleSignup = this.toggleSignup.bind(this);
        this.showLogin = this.showLogin.bind(this);
        this.showSignup = this.showSignup.bind(this);
        this.showBuyAirtime = this.showBuyAirtime.bind(this);
        this.showWallet = this.showWallet.bind(this);
        this.showFAQ = this.showFAQ.bind(this);
        this.showHelp = this.showHelp.bind(this);
        this.showMenu = this.showMenu.bind(this);
        this.renderView = this.renderView.bind(this);
    }

    componentDidMount() {
        this.setState({showMenu: true});
    }

    toggle(display){
        //toggle state of variable
        //console.log('In toggle')
        switch(display){
            case 'LOGIN':
                this.setState({
                    showMenu: false,
                    showBuyAirtime: false,
                    showSignup: false,
                    showLogin: true,
                    showWallet: false,
                    showFAQ: false,
                    showHelp: false
                });
                break;
            case 'SIGNUP':
                this.setState({
                    showMenu: false,
                    showBuyAirtime: false,
                    showSignup: true,
                    showLogin: false,
                    showWallet: false,
                    showFAQ: false,
                    showHelp: false
                });
                break;
            case 'RECHARGE':
                this.setState({
                    showMenu: false,
                    showBuyAirtime: true,
                    showSignup: false,
                    showLogin: false,
                    showWallet: false,
                    showFAQ: false,
                    showHelp: false
                });
                break;
            case 'FUND_WALLET':
                this.setState({
                    showMenu: false,
                    showBuyAirtime: false,
                    showSignup: false,
                    showLogin: false,
                    showWallet: true,
                    showFAQ: false,
                    showHelp: false
                });
                break;
            case 'FAQS':
                this.setState({
                    showMenu: false,
                    showBuyAirtime: false,
                    showSignup: false,
                    showLogin: false,
                    showWallet: false,
                    showFAQ: true,
                    showHelp: false
                });
                break;
            case 'SUPPORT':
                this.setState({
                    showMenu: false,
                    showBuyAirtime: false,
                    showSignup: false,
                    showLogin: false,
                    showWallet: false,
                    showFAQ: false,
                    showHelp: true
                });
                break;
            case '':
            default:
                this.setState({
                    showMenu: true,
                    showBuyAirtime: false,
                    showSignup: false,
                    showLogin: false,
                    showWallet: false,
                    showFAQ: false,
                    showHelp: false
                });
        }
        //if login and signup are false, set menu to true
        //else
    }

    showLogin() {
        //console.log('Login');
        return (
            <Login toggle={this.toggle} />
        )
    }

    showSignup() {
        //console.log('Signup');
        return (
            <Signup toggle={this.toggle} />
        )
    }

    showBuyAirtime() {
        //console.log('BuyAirtime');
        return (
            <BuyAirtime toggle={this.toggle} />
        )
    }

    showWallet() {
        //console.log('Wallet');
    }

    showFAQ() {
        //console.log('FAQ');
    }

    showHelp() {
        //console.log('Help');
    }

    showMenu(){
        return (
            <div>
                <Row>
                    <Col s={12} m={4}>
                        <MenuCard
                            color="blue darken-1"
                            title="Buy Airtime"
                            onClick={this.toggle.bind(null, 'RECHARGE')}
                            icon="credit_card"
                        />
                    </Col>
                    <Col s={12} m={4}>
                        <MenuCard
                            color="amber darken-1"
                            title="Signup"
                            onClick={this.toggle.bind(null, 'SIGNUP')}
                            icon="store"
                        />
                    </Col>
                    <Col s={12} m={4}>
                        <MenuCard
                            color="purple lighten-1"
                            title="Login"
                            onClick={this.toggle.bind(null, 'LOGIN')}
                            icon="toc"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col s={12} m={4}>
                        <MenuCard
                            color="teal lighten-1"
                            title="Fund Wallet"
                            onClick={this.toggle.bind(null, 'FUND_WALLET')}
                            icon="settings_cell"
                        />
                    </Col>
                    <Col s={12} m={4}>
                        <MenuCard
                            color="blue-grey darken-1"
                            title="FAQs"
                            onClick={this.toggle.bind(null, 'FAQs')}
                            icon="work"
                        />
                    </Col>
                    <Col s={12} m={4}>
                        <MenuCard
                            color="deep-purple darken-1"
                            title="Help & Support"
                            onClick={this.toggle.bind(null, 'SUPPORT')}
                            icon="perm_phone_msg"
                        />
                    </Col>
                </Row>
            </div>
        )
    }

    renderView() {
        if (this.state.showLogin) {
            return this.showLogin()
        }else if (this.state.showSignup) {
            return this.showSignup()
        }else if (this.state.showBuyAirtime) {
            return this.showBuyAirtime()
        }else if (this.state.showWallet) {
            return this.showWallet()
        }else if (this.state.showFAQ) {
            return this.showFAQ()
        }else if (this.state.showHelp) {
            return this.showHelp()
        }else {
            return this.showMenu()
        }
    }

    render() {
        //console.log('rendering Home');

        return (
            <Row>
                <Col s={12} m={6} l={6}>
                    {
                        this.renderView()
                    }
                </Col>
                <Col s={12} m={6} l={6}>
                    <Card
                        header={
                            <CardTitle
                                reveal
                                image='images/Slide1.jpg'
                                waves='light'
                            />
                        }
                    >
                        <h2 className="center-align">Welcome to Poploda</h2>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default Home;