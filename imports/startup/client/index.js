import './routes';

//code to make FlowRouter to wait
FlowRouter.wait();

Tracker.autorun(() => {
    if (Roles.subscription.ready() && !FlowRouter._initialized) {
        FlowRouter.initialize();
    }
})