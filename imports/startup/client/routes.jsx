import React from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
import { Roles } from 'meteor/alanning:roles';
import { mount } from 'react-mounter';

import AppLayout from '../../ui/layouts/AppLayout';
import UserLayout from '../../ui/layouts/UserLayout';
import HelpLayout from '../../ui/layouts/HelpLayout';
import AdminLayout from '../../ui/layouts/AdminLayout';
//import Home from "../../ui/pages/Home";

//import HomeMenu from "../../ui/components/HomeMenu";
import DashboardMenu from "../../ui/components/DashboardMenu";
import Login from "../../ui/components/authentication/Login";
import Signup from "../../ui/components/authentication/Signup";
import BuyAirtime from '../../ui/components/users/BuyAirtime';
import PurchaseComplete from '../../ui/components/users/PurchaseComplete';
import History from '../../ui/components/users/UserHistory';
import UserProfile from '../../ui/components/users/UserProfile';
import ChangePassword from '../../ui/components/users/ChangePassword';
import FAQ from '../../ui/components/FAQ';
import Support from '../../ui/components/Support';

import UserListContainer from '../../ui/components/admin/UserListContainer';
import AirtimePurchaseContainer from '../../ui/components/admin/AirtimePurchaseContainer';
import WalletTopup from '../../ui/components/admin/WalletTopupForm';
import EmailBlastContainer from '../../ui/components/admin/EmailBlastContainer';
import AdminHomeContainer from '../../ui/components/admin/AdminHomeContainer';

//import UserDashboard from '../../ui/components/users/UserDashboard';
import UserHome from '../../ui/components/users/UserHome';
import UserMenu from '../../ui/components/users/UserMenu';
import AdminMenu from '../../ui/components/admin/AdminMenu';


function checkLoggedIn(ctx, redirect){
   if(!Meteor.userId()){
       //console.log('not logged in, redirecting')
       redirect('/')
   }
}

function loggedIn(ctx, redirect){
    if(Meteor.userId()){
        //console.log('logged in, redirecting')
        redirect('/user')
        //FlowRouter.go('user')
    }
}

function checkAdminLoggedIn(ctx, redirect){
    if(!Roles.userIsInRole(Meteor.userId(), 'admin')){
        redirect('/user')
    }
}

let userRoutes = FlowRouter.group({
    prefix: '/user',
    name: 'user',
    triggersEnter: [checkLoggedIn]
});

let adminRoutes = FlowRouter.group({
    prefix: '/admin',
    name: 'admin',
    triggersEnter: [checkAdminLoggedIn]
});

FlowRouter.route('/', {
    name: 'dashboard',
    action() {
        //console.log('rendering dashboard')
        mount(AppLayout, {
            content: <DashboardMenu />
        });
    },
    triggersEnter: [loggedIn]
});

FlowRouter.route('/complete', {
    name: 'purchase-complete',
    action() {
        mount(AppLayout, {
            content: <PurchaseComplete />
        });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action() {
        mount(AppLayout, {
            content: <Login />
        });
    }
});

FlowRouter.route('/signup', {
    name: 'signup',
    action() {
        mount(AppLayout, {
            content: <Signup />
        });
    }
});

FlowRouter.route('/faqs', {
    name: 'faqs',
    action() {
        mount(HelpLayout, {
            content: <FAQ />
        });
    }
});

FlowRouter.route('/support', {
    name: 'support',
    action() {
        mount(HelpLayout, {
            content: <Support />
        });
    }
});

FlowRouter.route('/recharge', {
    name: 'buy-airtime',
    action() {
        mount(AppLayout, {
            content: <BuyAirtime />
        });
    }
})
//
// FlowRouter.route('/fund-wallet', {
//     name: 'fund-wallet',
//     action() {
//         mount(AppLayout, {
//             content: <Login />
//         });
//     }
// })
//



userRoutes.route('/', {
    name: 'user-home',
    action() {
        mount(UserLayout, {
            menu: <UserMenu />,
            content: <UserHome />
        });
    }
});

userRoutes.route('/history', {
    name: 'user-history',
    action() {
        mount(UserLayout, {
            menu: <UserMenu />,
            content: <History />
        });
    }
});

userRoutes.route('/wallet', {
    name: 'user-wallet',
    action() {
        mount(UserLayout, {
            menu: <UserMenu />,
            content: <Wallet />
        });
    }
});

userRoutes.route('/recharge', {
    name: 'user-recharge',
    action() {
        mount(UserLayout, {
            menu: <UserMenu />,
            content: <BuyAirtime />
        });
    }
});

userRoutes.route('/profile', {
    name: 'user-profile',
    action() {
        mount(UserLayout, {
            menu: <UserMenu />,
            content: <UserProfile />
        });
    }
});

userRoutes.route('/change-password', {
    name: 'change-password',
    action() {
        mount(UserLayout, {
            menu: <UserMenu />,
            content: <ChangePassword />
        });
    }
});

//ADMIN ROUTES
adminRoutes.route('/', {
    name: 'admin-dashboard',
    action() {
        mount(AdminLayout, {
            menu: <AdminMenu />,
            content: <AdminHomeContainer />
        });
    }
});

adminRoutes.route('/users', {
    name: 'admin-users',
    action() {
        mount(AdminLayout, {
            menu: <AdminMenu />,
            content: <UserListContainer />
        });
    }
});

adminRoutes.route('/airtime-purchase', {
    name: 'admin-airtime-purchase',
    action() {
        mount(AdminLayout, {
            menu: <AdminMenu />,
            content: <AirtimePurchaseContainer />
        });
    }
});

adminRoutes.route('/wallet-topup', {
    name: 'admin-wallet-topup',
    action() {
        mount(AdminLayout, {
            menu: <AdminMenu />,
            content: <WalletTopup />
        });
    }
});

adminRoutes.route('/email-blast', {
    name: 'admin-email-blast',
    action() {
        mount(AdminLayout, {
            menu: <AdminMenu />,
            content: <EmailBlastContainer />
        });
    }
});










// function checkLoggedIn(ctx, redirect){
//     if(!Meteor.userId()){
//         //console.log('not logged in, redirecting')
//         redirect('/')
//     }
// }
//
// function loggedIn(ctx, redirect){
//     if(Meteor.userId()){
//         //console.log('logged in, redirecting')
//         redirect('/user')
//         //FlowRouter.go('user')
//     }
// }
//
// function checkAdminLoggedIn(ctx, redirect){
//     if(Meteor.userId()){
//         console.log('logged in as admin, redirecting')
//         redirect('/')
//     }
// }
//
// let userRoutes = FlowRouter.group({
//     prefix: '/user',
//     name: 'user',
//     triggersEnter: [checkLoggedIn]
// });
//
// let adminRoutes = FlowRouter.group({
//     prefix: '/admin',
//     name: 'admin',
//     triggersEnter: [checkAdminLoggedIn]
// });
//
// FlowRouter.route('/', {
//     name: 'dashboard',
//     action() {
//         //console.log('rendering dashboard')
//         mount(AppLayout, {
//             content: <DashboardMenu />
//         });
//     },
//     triggersEnter: [loggedIn]
// });
//
// FlowRouter.route('/complete', {
//     name: 'purchase-complete',
//     action() {
//         mount(AppLayout, {
//             content: <PurchaseComplete />
//         });
//     }
// });
//
// FlowRouter.route('/login', {
//     name: 'login',
//     action() {
//         mount(AppLayout, {
//             content: <Login />
//         });
//     }
// });
//
// FlowRouter.route('/signup', {
//     name: 'signup',
//     action() {
//         mount(AppLayout, {
//             content: <Signup />
//         });
//     }
// });
//
// FlowRouter.route('/faqs', {
//     name: 'faqs',
//     action() {
//         mount(HelpLayout, {
//             content: <FAQ />
//         });
//     }
// });
//
// FlowRouter.route('/support', {
//     name: 'support',
//     action() {
//         mount(HelpLayout, {
//             content: <Support />
//         });
//     }
// });
//
// FlowRouter.route('/recharge', {
//     name: 'buy-airtime',
//     action() {
//         mount(AppLayout, {
//             content: <BuyAirtime />
//         });
//     }
// })
//
// userRoutes.route('/', {
//     name: 'user-home',
//     action() {
//         mount(UserLayout, {
//             menu: <UserMenu />,
//             content: <UserHome />
//         });
//     }
// });
//
// userRoutes.route('/history', {
//     name: 'user-history',
//     action() {
//         mount(UserLayout, {
//             menu: <UserMenu />,
//             content: <History />
//         });
//     }
// });
//
// userRoutes.route('/wallet', {
//     name: 'user-wallet',
//     action() {
//         mount(UserLayout, {
//             menu: <UserMenu />,
//             content: <Wallet />
//         });
//     }
// });
//
// userRoutes.route('/recharge', {
//     name: 'user-recharge',
//     action() {
//         mount(UserLayout, {
//             menu: <UserMenu />,
//             content: <BuyAirtime />
//         });
//     }
// });
//
// userRoutes.route('/profile', {
//     name: 'user-profile',
//     action() {
//         mount(UserLayout, {
//             menu: <UserMenu />,
//             content: <UserProfile />
//         });
//     }
// });
//
// userRoutes.route('/change-password', {
//     name: 'change-password',
//     action() {
//         mount(UserLayout, {
//             menu: <UserMenu />,
//             content: <ChangePassword />
//         });
//     }
// });
//
// //ADMIN ROUTES
// adminRoutes.route('/', {
//     name: 'admin-dashboard',
//     action() {
//         mount(AdminLayout, {
//             menu: <AdminMenu />,
//             content: <UserHome />
//         });
//     }
// });
//
// adminRoutes.route('/users', {
//     name: 'users',
//     action() {
//         mount(AdminLayout, {
//             menu: <AdminMenu />,
//             content: <UserListContainer />
//         });
//     }
// });
//
// adminRoutes.route('/airtime-purchase', {
//     name: 'airtime-purchase',
//     action() {
//         mount(AdminLayout, {
//             menu: <AdminMenu />,
//             content: <AirtimePurchaseContainer />
//         });
//     }
// });
//
// adminRoutes.route('/wallet-topup', {
//     name: 'wallet-topup',
//     action() {
//         mount(AdminLayout, {
//             menu: <AdminMenu />,
//             content: <WalletTopup />
//         });
//     }
// });
//
// adminRoutes.route('/email-blast', {
//     name: 'email-blast',
//     action() {
//         mount(AdminLayout, {
//             menu: <AdminMenu />,
//             content: <EmailBlastContainer />
//         });
//     }
// });







