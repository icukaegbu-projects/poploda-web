import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
//import '../imports/api/users/methods';
import '../imports/startup/server';
import './publications';

Meteor.startup(() => {
  // code to run on server at startup
    process.env.MAIL_URL="smtp://icukaegbu%40gmail.com:ikedi2012Chibueze@smtp.googlemail.com:465/"
    
    //check if admin-user is created, if no create
    if(!Meteor.users.find().count()) {
        var options = {
            username: 'admin',
            password: Meteor.settings.private.ADMIN_PASSWORD,
            email: Meteor.settings.private.ADMIN_USER,
            bio: {
                name: 'Admin',
                phone: Meteor.settings.private.ADMIN_PHONE,
                network: Meteor.settings.private.ADMIN_NETWORK
            }
        };

        let user = Accounts.createUser(options);

        Roles.addUsersToRoles(user, 'admin');
    }
});
